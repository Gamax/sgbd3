# SGBD3

Repo du cours de SGBD3 de 3ème année à l'inpres

## ALIM_CB

Nom du package :  BemelmansDethierPackage

### Procédure 1 : AlimFromRandom

génére tableau d'id aléatoire présent dans la base

### Procédure 2 : AlimFromFile

charger ids depuis fichier et générer tableau

### Procédure 3 : AlimFromIDTable

commune importe ces ids depuis movies_ext dans la bdd

## CREATE_CB



## RechCC

- DBLink > simple procédure de cc accède au table de cb
- ords, param de la requete http donné direct en param de la fonction sur cc
  - Requete avec critère qui donne liste de film en retour (ne pas importer cb -> cc) POST
  - Requete avec id pour avoir détail qui donne détails film en retour (importer cb -> cc) POST
  - Requete sur id pour voir si le film existe ou non via GET
- java swing qui fait appel http

#### Recherche critères

- jusqu'a 3 acteurs
- jusqu'a 2 réalisateurs

## ORDS

### Mise en route

``java -jar ords.war standalone`` dans le dossier ords

### Fonctionnement

``http://localhost:9082/ords/cc/pkgname.procedure`` Cette requete va faire appel à la procédure procedure de pkgname avec les paramètres la requète get et post vont être mappé au paramètres de la fonction en fonction du nom. Les paramètres doivent être de type varchar2. Ce package doit appartenir à CC !

La réponse est formattée comme ceci : 

````sql
owa_util.status_line(nstatus => 200, bclose_header => false); 
--header 200 = statut http

owa_util.mime_header ( 
    'text/html',
    bclose_header => true,
    ccharset => 'UTF-8'
);
--ne pas changer info du header
htp.print('corps réponse en string'); --contenu de la réponse ! Appel print successifs
````



