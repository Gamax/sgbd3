declare
  json varchar2(32767)
  := '{
  "title": "Titre",
  "original_title": "Titre Original",
  "actors": [
    {
      "name": "Nom Acteur 1"
    }
  ],
  "directors": [
    {
      "name": "Nom Directeur"
    },
    {
      "name": "Nom Directeur"
    }
  ],
  "period": {
    "year": "2000",
    "time": "before ou after ou during"
  }
}';

begin
apex_json.parse(json);
dbms_output.put_line(apex_json.get_varchar2('title'));
dbms_output.put_line(apex_json.get_varchar2('actors[%d].name',1));

if(apex_json.get_varchar2('actors[%d].name',2) IS null) then
  dbms_output.put_line('egale à nul');
  else
  dbms_output.put_line(apex_json.get_varchar2('actors[%d].name',2));
end if;


end;