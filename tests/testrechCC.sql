drop table TEST_RECHCC_MOVIE cascade constraints;

create table test_rechcc_movie
(
  id             number(6),
  title          varchar2(60 char)
    CONSTRAINT test_rechcc_movie$NN$title NOT NULL,
  original_title varchar2(60 char)
    CONSTRAINT test_rechcc_movie$NN$O_title NOT NULL,
  status         number(1)
    CONSTRAINT test_rechcc_movie$FK$status REFERENCES status (id),
  release_date   date,
  constraint test_rechcc_movie$pk primary key (id)
);

declare
  title_query          varchar2(1000);
  year                 integer := 2000;
  time                 integer := -2;
  actors_1             varchar2(1000);
  actors_2             varchar2(1000);
  actors_3             varchar2(1000);
  directors_1          varchar2(1000);
  directors_2          varchar2(1000);
  refcursor sys_refcursor;
  id movie.id%type;
  title movie.title%type;
  original_title movie.original_title%type;
  status movie.status%type;
  release_date movie.release_date%type;
begin
  open refcursor for
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  WHERE title_query IS NULL
     OR UPPER(TITLE) LIKE UPPER('%' || title_query || '%')
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  WHERE title_query IS NULL
     OR UPPER(ORIGINAL_TITLE) LIKE UPPER('%' || title_query || '%')
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where actors_1 is null
     or ID IN
        (
          select MOVIE
          from ARTIST
                 inner join movie_actor on ARTIST.ID = MOVIE_ACTOR.ACTOR
          where upper(NAME) = upper(actors_1)
        )
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where actors_2 is null
     or ID IN
        (
          select MOVIE
          from ARTIST
                 inner join movie_actor on ARTIST.ID = MOVIE_ACTOR.ACTOR
          where upper(NAME) = upper(actors_2)
        )
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where actors_3 is null
     or ID IN
        (
          select MOVIE
          from ARTIST
                 inner join movie_actor on ARTIST.ID = MOVIE_ACTOR.ACTOR
          where upper(NAME) = upper(actors_3)
        )
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where directors_1 is null
     or ID IN
        (
          select MOVIE
          from ARTIST
                 inner join MOVIE_DIRECTOR on ARTIST.ID = MOVIE_DIRECTOR.DIRECTOR
          where upper(NAME) = upper(directors_1)
        )
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where directors_2 is null
     or ID IN
        (
          select MOVIE
          from ARTIST
                 inner join MOVIE_DIRECTOR on ARTIST.ID = MOVIE_DIRECTOR.DIRECTOR
          where upper(NAME) = upper(directors_2)
        )
  INTERSECT
  select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
  from MOVIE@cblink
  where year is null
     or
        time = -2
    or
       time = -1 and year > extract(year from MOVIE.RELEASE_DATE)
         or
       time = 0 and year = extract(year from MOVIE.RELEASE_DATE)
         or
       time = 1 and year < extract(year from MOVIE.RELEASE_DATE)
    order by ID;

   LOOP
      FETCH refcursor into id,title,original_title,status,release_date;
      EXIT WHEN refcursor%notfound;
      DBMS_OUTPUT.put_line(id || ' ' || title);
   END LOOP;
   close refcursor;

end;

declare
  id_int integer := 20;
  begin
  select id
       from movie
         where id_int = id;
end;

select id
       from movie@cblink
         where 13 = id;

SELECT acb.ID as ID, acb.NAME as NAME
          from MOVIE_ACTOR@cblink macb
          inner join ARTIST@cblink acb
          on macb.ACTOR = acb.ID
          where macb.MOVIE = 13;

select m.ID,ORIGINAL_TITLE,RELEASE_DATE,s2.NAME,VOTE_AVERAGE,VOTE_COUNT,RUNTIME,c2.NAME
        from MOVIE m
          inner join CERTIFICATION C2 on m.CERTIFICATION = C2.ID
          inner join STATUS S2 on m.STATUS = S2.ID
        where m.id=13;

select name
from ARTIST a
inner join MOVIE_DIRECTOR md on a.ID = md.DIRECTOR
where md.MOVIE = 13;

select name from GENRE g
                         inner join MOVIE_GENRE mg on g.ID = mg.GENRE where mg.MOVIE = 13;

begin
  RECHCC_PKG.IMPORTCBTOCC(13);
  commit;
end;