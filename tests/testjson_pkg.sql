create or replace package json_pkg is
  procedure test(message varchar2);
end json_pkg;
create or replace package body json_pkg is
  procedure test(message varchar2) is
    begin
      owa_util.status_line(nstatus => 200, bclose_header => false);
      owa_util.mime_header(
          'text/html',
          bclose_header => true,
          ccharset => 'UTF-8'
      );
      htp.PRINT(message);
    end test;
end json_pkg;