/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JSON;

import Utils.*;
import java.util.List;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 *
 * @author Steph
 */
public class LectureJSON_RQSOriented {
    
    
    private final String URL = "http://localhost:9082/ords/cc/" ; 
    
    public LectureJSON_RQSOriented()
    {
         
    }

    
    
    public JSONObject sendRequestPost(JSONObject jsonSend, String url) throws JSONException, MalformedURLException, ProtocolException, IOException
    {
        System.out.println(jsonSend.toString());
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost(url);
        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("message", jsonSend.toString()));
        request.addHeader("content-type", "application/x-www-form-urlencoded");
        request.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
        
        HttpResponse result = httpClient.execute(request);
        String json = EntityUtils.toString(result.getEntity(), "UTF-8");
        JSONObject jsonReceive= new JSONObject(json);
        
        
        return jsonReceive;
    }
    
    public JSONObject sendRequestGet(String url) throws JSONException, MalformedURLException, IOException
    {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        
        
        HttpResponse result = httpClient.execute(request);
        String json = EntityUtils.toString(result.getEntity(), "UTF-8");
        JSONObject jsonReceive= new JSONObject(json);
        
        return jsonReceive;
    }
    
    public Movie getFilmDetail(String id) throws JSONException, IOException
    {
        Movie mov = new Movie ();
        
        String url = URL + "rechcc_pkg.rechID?id="+id ;
        JSONObject jsonReceive = sendRequestGet(url) ;
        mov.setID(id);
        try {
            mov.setTitle(jsonReceive.getString("title"));
        }catch (JSONException e) {
            mov.setTitle("/");
        }
        try {
            mov.setOriginalTitle(jsonReceive.getString("original_title"));
        }catch (JSONException e) {
            mov.setOriginalTitle("/");
        }
        try {
        mov.setDate(jsonReceive.getString("release_date"));
        }catch (JSONException e) {
            mov.setDate("/");
        }
        try {
        mov.setStatus(jsonReceive.getString("status"));
        }catch (JSONException e) {
            mov.setStatus("/");
        }
        try {
        mov.setPoster(jsonReceive.getString("poster"));
        }catch (JSONException e) {
            mov.setPoster("/");
        }
        try {
        mov.setCertification(jsonReceive.getString("certification"));
        }catch (JSONException e) {
            mov.setCertification("/");
        }
        try {
        mov.setRuntime(String.valueOf(jsonReceive.get("runtime")));
        }catch (JSONException e) {
            mov.setRuntime("/");
        }
        try {
        mov.setVoteAverage(String.valueOf(jsonReceive.get("rqs_votes_average")));
        }catch (JSONException e) {
            mov.setVoteAverage("/");
        }
        try {
        mov.setVoteCount(String.valueOf(jsonReceive.get("rqs_votes_number")));
        }catch (JSONException e) {
            mov.setVoteCount("/");
        }
        try {
        mov.setVoteAverageTMDB(String.valueOf(jsonReceive.get("tmdb_votes_average")));
        }catch (JSONException e) {
            mov.setVoteAverageTMDB("/");
        }
        try {
        mov.setVoteCountTMDB(String.valueOf(jsonReceive.get("tmdb_votes_number")));
        }catch (JSONException e) {
            mov.setVoteCountTMDB("/");
        }
        
        
        int i ; 
        try{
            JSONArray acteurs = jsonReceive.getJSONArray("actors");
            for(i = 0 ; i < acteurs.length() ; i++)
            {
                mov.addActeur(acteurs.getJSONObject(i).getString("name"));
            }
        }catch (JSONException e) {
            
        }
        try{
            JSONArray realisateurs = jsonReceive.getJSONArray("directors");
            for(i = 0 ; i < realisateurs.length() ; i++)
            {
                mov.addRealisateur(realisateurs.getJSONObject(i).getString("name"));
            }
        }catch (JSONException e) {
            
        }
        try{
            JSONArray genres = jsonReceive.getJSONArray("genres");
            for(i = 0 ; i < genres.length() ; i++)
            {
                mov.addGenre(genres.getJSONObject(i).getString("name"));
            }
        }catch (JSONException e) {
            
        }
        
        return mov ; 
    }
    
    public ArrayList<Movie> getMovies(Criteres criteres) throws JSONException, MalformedURLException, IOException
    {        
        // <editor-fold defaultstate="collapsed" desc="generation JSON avec criteres">
        System.out.println(criteres.toString());
        if(criteres.isEmpty())
            return null ; 
        
        JSONObject jsonSend = new JSONObject() ;
        ArrayList<Movie> films = new ArrayList<>();
        Movie tmp ;
        
        JSONArray jsA ;
        JSONObject jsO ;
        if(!criteres.getActeurs().isEmpty())
        {
            jsA = new JSONArray();
            
            for(int i = 0 ; i < criteres.getActeurs().size() ; i++)
            {
                jsO = new JSONObject();
                jsO.put("name",criteres.getActeurs().get(i)) ;
                jsA.put(jsO);
            }
            
            jsonSend.put("actors", jsA);
        }
        
        if(!criteres.getRealisateurs().isEmpty())
        {
            jsA = new JSONArray();
            
            for(int i = 0 ; i < criteres.getRealisateurs().size() ; i++)
            {
                jsO = new JSONObject();
                jsO.put("name",criteres.getRealisateurs().get(i)) ;
                jsA.put(jsO);
            }
            
            jsonSend.put("directors", jsA);
        }
        
        if(criteres.getDate() != null)
        {
            Map<String, String> date = new HashMap<String, String>();
            date.put("year", criteres.getDate());
            date.put("time", criteres.getRelativeDate());
            jsonSend.put("period",date);
        }
        
        if(criteres.getTitle() != null)
        {
            jsonSend.put("title", criteres.getTitle());
        }
        // </editor-fold> 
        
        String url = URL + "rechcc_pkg.rechCritere" ;
        JSONObject jsonReceive = sendRequestPost(jsonSend, url);
        
        JSONArray jsonMovies = jsonReceive.getJSONArray("list");
        JSONObject jsonMovie ;
        int cpt = jsonMovies.length();
        
        for(int i = 0 ; i < cpt ; i++)
        {
            jsonMovie = jsonMovies.getJSONObject(i);
            tmp = new Movie();
            tmp.setID(String.valueOf(jsonMovie.get("id")));

            try {
                tmp.setTitle(jsonMovie.getString("title"));
            } catch (JSONException e) {
                tmp.setTitle("/");
            }
            try {
                tmp.setOriginalTitle(jsonMovie.getString("original_title"));
            } catch (JSONException e) {
                tmp.setOriginalTitle("/");
            }
            try {
                tmp.setDate(jsonMovie.getString("release_date"));
            } catch (JSONException e) {
                tmp.setDate("/");
            }
            try{
                tmp.setStatus(jsonMovie.getString("status"));
            }catch (JSONException e) {
                tmp.setStatus("/");
            }
            
            films.add(tmp);
        }
        return films ;
    }
    
    
    

    public Moyenne getNewMoyenne(UserVote vote, String action) throws JSONException, ProtocolException, IOException
    {
        String url = URL + "evalCC.eval";
        JSONObject jsonSend = new JSONObject() ;
        Moyenne moy = new Moyenne();
        if(vote.getMovie()==null || vote.getMovie().length() < 1)
            return null ;
        if(vote.getUsername()==null || vote.getUsername().length() < 1)
            return null ;
        if(vote.getPassword()==null || vote.getPassword().length() < 1)
            return null ;
        if(action != "add" && action != "delete")
            return null ; 
        if(vote.getNote() < 0 || vote.getNote() > 10)
            return null ; 
        jsonSend.put("movie", vote.getMovie());
        jsonSend.put("action", action);
        jsonSend.put("username", vote.getUsername());
        jsonSend.put("password", vote.getPassword());
        jsonSend.put("vote", vote.getNote());
        
        if(vote.getAvis() != null)
            jsonSend.put("avis", vote.getAvis());
        
        
        JSONObject jsonReceive = sendRequestPost(jsonSend, url);
        moy.setMoyenne(jsonReceive.getDouble("rqs_votes_average"));
        moy.setCount(jsonReceive.getInt("rqs_votes_number"));
        
        return moy ; 
    }

    public UserVote getAvis(String user, String movie) throws JSONException, ProtocolException, IOException
    {
        String url = URL + "evalCC.avis";
        UserVote vote = new UserVote();
        JSONObject jsonSend = new JSONObject() ;
        
        jsonSend.put("username", user);
        jsonSend.put("movie",movie);
      
        JSONObject jsonReceive = sendRequestPost(jsonSend, url);
      
        System.out.println(jsonReceive.toString());
        vote.setMovie(movie);
        vote.setUsername(user);
        
        vote.setNote(jsonReceive.getInt("vote"));
        vote.setAvis(jsonReceive.getString("avis"));
      
        return vote ;
    }
    
}
