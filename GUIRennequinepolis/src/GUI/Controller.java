/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI;

import Utils.Criteres;
import JSON.LectureJSON_RQSOriented;
import Utils.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.json.JSONException;

/**
 *
 * @author Steph
 */
public class Controller {
    private GUIAccueil accueil ;
    private GUICriteres filmsByCriteres ;
    private GUIFilm filmDetail ;
    private GUIEval evaluation ;
    
    private LectureJSON_RQSOriented requestInfo ; 
    private Criteres criteres ;    
    private ArrayList<Movie> listeFilms ;
    private Movie filmEnCours ;
    private UserVote vote ; 
    
    
    public Controller()
    {
        criteres = new Criteres();
        requestInfo = new LectureJSON_RQSOriented();
        listeFilms = null ; 
        filmEnCours = null ; 
        vote = new UserVote() ;
        Accueil_ouverture();
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="ACCUEIL">
    public void Accueil_ouverture()
    {
        accueil = new GUIAccueil();
        accueil.setController(this);
        accueil.setVisible(true);
    }
    
    public void rechercherID(String ID)
    {
        setDetailFilm(ID);
        accueil.setVisible(!Detail_Ouverture());
    }
    
    public void rechercherCriteres(Criteres crit)
    {
       criteres = crit ;
       Criteres_Ouverture();
       
    }
    
    public void Accueil_fermeture()
    {
        System.out.println("Fermeture !");
        accueil.setVisible(false);
        accueil.dispose();
        accueil = null ; 
    }
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="LISTE PAR CRITERES">
    public void Criteres_Ouverture()
    {
        filmsByCriteres = new GUICriteres();
        filmsByCriteres.setController(this);
        
        filmsByCriteres.setListe(getFilms());
        
        filmsByCriteres.setVisible(true);
        accueil.setVisible(false);
    }
    
    public void Criteres_Selection(int index)
    {
        setDetailFilm(getId(index));
        filmsByCriteres.setVisible(!Detail_Ouverture());
    }
    
    public void Criteres_fermeture()
    {
        filmsByCriteres.setVisible(false);
        filmsByCriteres.dispose();
        filmsByCriteres = null ; 
        Accueil_ouverture();
    }
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="DETAIL FILM">
    public boolean Detail_Ouverture()
    {        
        if(filmEnCours == null)
        {
            JOptionPane.showMessageDialog(null, "Aucun film ne correspond à cet identifiant !","Erreur",JOptionPane.ERROR_MESSAGE );
            return false ;
        }
        else
        {
            filmDetail = new GUIFilm();
            filmDetail.setController(this);
            
            filmDetail.setTitre(filmEnCours.getTitle());
            filmDetail.setTitreOriginal(filmEnCours.getOriginalTitle());
            filmDetail.setDate(filmEnCours.getDate());
            filmDetail.setStatus(filmEnCours.getStatus());
            filmDetail.setDuree(filmEnCours.getRuntime());
            filmDetail.setCertification(filmEnCours.getCertification());
            filmDetail.setGenres(getGenres());
            filmDetail.setTMDBMoyenne(filmEnCours.getVoteAverageTMDB());
            filmDetail.setTMBDNbVotes(filmEnCours.getVoteCountTMDB());
            filmDetail.setMoyenne(filmEnCours.getVoteAverage());
            filmDetail.setNbVotes(filmEnCours.getVoteCount());
            filmDetail.setActeurs(getActeurs());
            filmDetail.setRealisateurs(getRealisateurs());
            filmDetail.setAffiche(filmEnCours.getTitle());
            
            filmDetail.setVisible(true);
            return true ;
            
       }
    }
    
    public void Detail_evaluer()
    {
       Eval_ouverture();
    }
    
    public void Detail_fermeture()
    {
        if(filmsByCriteres == null)
            accueil.setVisible(true);
        else
            filmsByCriteres.setVisible(true);
        filmDetail.setVisible(false);
        filmDetail.dispose();
        filmDetail=null;
        
    }
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="EVAL">
    public void Eval_ouverture()
    {
        try {
            evaluation = new GUIEval();
            evaluation.setController(this);
            evaluation.setVisible(true);
            vote.setMovie(filmEnCours.getID());
            if(!vote.getUsername().isEmpty())
                getAvis();
            evaluation.setValues(vote.getUsername(),vote.getPassword(),vote.getAvis(),vote.getNote());
        } catch (IOException | JSONException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void Eval_fermeture()
    {
        evaluation.setVisible(false);
        evaluation.dispose();
        evaluation=null;
    }
    
    public void Eval_Ajouter(String user, String password, int note, String avis)
    {
        vote.setUsername(user);
        vote.setPassword(password);
        vote.setNote(note);
        vote.setAvis(avis);
        if(setNewVote("add"))
             Eval_fermeture();

    }
    
    public void Eval_Supprimer(String user, String password, int note, String avis)
    {
        vote.setUsername(user);
        vote.setPassword(password);
        vote.setNote(note);
        vote.setAvis(avis);
        
        if(setNewVote("delete"))
            Eval_fermeture();
    }
    // </editor-fold>   
    
    // <editor-fold defaultstate="collapsed" desc="controller">
    public void setFilms()
    {
        try {
            listeFilms = requestInfo.getMovies(criteres);
        } 
        catch (IOException | JSONException e) 
        {
            System.out.println("ERROR : " + e.getMessage());
            listeFilms = null ;
        }
        
    }
    
    public void setDetailFilm(String id)
    {
        try {
            filmEnCours = requestInfo.getFilmDetail(id);
        } 
        catch (IOException | JSONException e) 
        {
            System.out.println("ERROR : " + e.getMessage());
            filmEnCours = null ;
        }
    }
    
    public String getId(int index)
    {
        if(listeFilms == null)
            System.out.println("Wut ?");
        
        if(listeFilms != null)
            return listeFilms.get(index).getID();
        else
            return null ;
    }
    
    public DefaultTableModel getFilms()
    {
        setFilms();
        if(listeFilms==null)
            return null ; 
        
        DefaultTableModel dtm = new DefaultTableModel();
        Vector vec ;
        
        dtm.addColumn("Titre");
        dtm.addColumn("Titre original");
        dtm.addColumn("Date de sortie");
        dtm.addColumn("Statut");
        
        
        
        for(int i = 0 ; i < listeFilms.size() ; i++)
        {
            vec = new Vector();
            
            vec.add(listeFilms.get(i).getTitle());
            vec.add(listeFilms.get(i).getOriginalTitle());
            vec.add(listeFilms.get(i).getDate());
            vec.add(listeFilms.get(i).getStatus());
            
            dtm.addRow(vec);
        }
        
        return dtm ;
    }
    
    public DefaultListModel getActeurs()
    {
        if(filmEnCours==null)
            return null ; 
        DefaultListModel dlm = new DefaultListModel();
        int cpt = filmEnCours.getActeurs().size();
        
        for(int i = 0 ; i < cpt ; i++)
        {
             dlm.addElement(filmEnCours.getActeurs().get(i));
        }
        
        return dlm ;
    }
    
    public DefaultListModel getRealisateurs()
    {
        if(filmEnCours==null)
            return null ; 
        DefaultListModel dlm = new DefaultListModel();
        int cpt = filmEnCours.getRealisateurs().size();
        
        for(int i = 0 ; i < cpt ; i++)
        {
             dlm.addElement(filmEnCours.getRealisateurs().get(i));
        }
        
        return dlm ;
    }
    
    public String getGenres()
    {
        if(filmEnCours==null)
            return null ; 
        StringBuilder str = new StringBuilder();
        
                
        try {
            int cpt = filmEnCours.getGenres().size();
           str.append(filmEnCours.getGenres().get(0)); 
        
        
        
        for(int i = 1 ; i < cpt ; i ++)
        {
            str.append(", ");
            str.append(filmEnCours.getGenres().get(i));
        }
        
        } catch (Exception e) {
            return "";
        }
        
        return str.toString();
    }
    
    public boolean setNewVote(String action)
    {
        try{
            Moyenne moyenne = requestInfo.getNewMoyenne(vote, action);

            filmDetail.setNbVotes(String.valueOf(moyenne.getCount()));
            filmDetail.setMoyenne(String.valueOf(moyenne.getMoyenne()));
            return true ; 
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Erreur : " + e.getMessage() + " Cause : " + e.getCause(),"Erreur",JOptionPane.ERROR_MESSAGE );
            return false ;
        }
    }
    
    public void getAvis() throws JSONException, IOException
    {
        String pswd = vote.getPassword();
        vote = requestInfo.getAvis(vote.getUsername(),vote.getMovie());
        vote.setPassword(pswd);
    }
    // </editor-fold> 
    
}
