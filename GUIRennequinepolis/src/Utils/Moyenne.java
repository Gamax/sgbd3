/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Steph
 */
public class Moyenne {
    private double cote_moyenne ;
    private int nb_votes ;
    
    public Moyenne()
    {
        
    }
    
    public Moyenne(double moyenne, int count)
    {
        cote_moyenne = moyenne ;
        nb_votes = count ;
    }
    
    public double getMoyenne() { return cote_moyenne ; }
    public int getCount() { return nb_votes ; }
    
    public void setMoyenne(double value) { cote_moyenne = value ; }
    public void setCount(int value) { nb_votes = value ; }
}
