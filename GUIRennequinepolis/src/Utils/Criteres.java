/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;

/**
 *
 * @author Steph
 */
public class Criteres {
    private ArrayList<String> acteurs ; 
    private ArrayList<String> realisateurs ;
    private String title ;
    private String date ; 
    private String relativeDate ; 
    
    public Criteres()
    {
        title = null ;
        date = null ;
        acteurs = new ArrayList<>();
        realisateurs = new ArrayList<>();
    }
    
    public Criteres(ArrayList<String> actors, ArrayList<String> real, String titre, String Date, String rel)
    {
        acteurs = new ArrayList<>(actors);
        realisateurs = new ArrayList<>(real) ;
        title = titre ;
        date = Date ;
        relativeDate = rel ;
    }
    
    public void setActeurs (ArrayList<String> value) { acteurs = new ArrayList<>(value) ; }
    public void setRealisateurs (ArrayList<String> value) { realisateurs = new ArrayList<>(value) ; }
    public void setTitle (String value) { title = value ; }
    public void setDate (String value) { date = value ; }
    public void setRelativeDate (String value) { relativeDate = value ; }
    
    public void addActeur(String value) { acteurs.add(value) ; }
    public void addRealisateur(String value) { realisateurs.add(value) ; }
    public void removeActeur(String value) { acteurs.remove(value); }
    public void removeRealisateur(String value) { realisateurs.remove(value); }
    
    public ArrayList<String> getActeurs () { return acteurs ; }
    public ArrayList<String> getRealisateurs () { return realisateurs ; }
    public String getTitle () { return title ; }
    public String getDate () { return date ; }
    public String getRelativeDate () { return relativeDate ; }
    
    public boolean isEmpty() //check si au moins un critere est defini
    {
        if(!acteurs.isEmpty())
            return false ;
        if(!realisateurs.isEmpty())
            return false ;
        if(date != null)
            return false ;
        if(title != null)
            return false ;
        return true ;
    }
    
    public String affiche()
    {
        StringBuilder str = new StringBuilder();
        if(!acteurs.isEmpty())
        {
            str.append("Acteurs : ");
            for(int i = 0 ; i < acteurs.size();i++)
            {
                str.append(acteurs.get(i) + " ; ");
            }
        }
        if(!realisateurs.isEmpty())
        {
            str.append("Réalisateurs : ");
            for(int i = 0 ; i < realisateurs.size();i++)
            {
                str.append(realisateurs.get(i) + " ; ");
            }
        }
        if(date != null)
        {
            str.append("date : ");
            str.append(relativeDate + " " + date + " ; ");
        }
        if(title != null)
        {
            str.append("Titre : ");
            str.append(title + " ; ");
        }
        return str.toString();
    }
}
