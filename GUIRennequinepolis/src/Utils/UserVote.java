/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

/**
 *
 * @author Steph
 */
public class UserVote {
    private String idMovie ;
    private String username ;
    private String mdp ;
    private int note ;
    private String avis ;
    
    public UserVote()
    {
        username = "" ;
    }
    
    public UserVote(String film, String user, String pswd)
    {
        username = user ;
        idMovie = film ;
        mdp = pswd ;
    }
    
    public UserVote(String film, String user, String pswd, int Note)
    {
        username = user ;
        note = Note ;
        idMovie = film ;
        mdp = pswd ;
    }
    
    public UserVote(String film, String user, String pswd, int Note, String Avis )
    {
        username = user ;
        note = Note ;
        avis = Avis ; 
        idMovie = film ;
        mdp = pswd ;
    }
    
    public UserVote(UserVote original)
    {
        idMovie = original.getMovie();
        username = original.getUsername() ;
        note = original.getNote();
        avis = original.getAvis();
        mdp = original.getPassword() ;
    }
    
    public void setMovie(String value) { idMovie = value ; }
    public void setUsername(String value) { username = value ; }
    public void setNote(int value) { note = value ; }
    public void setAvis(String value) { avis = value ; }
    public void setPassword(String value) { mdp = value ; }

    public String getMovie() { return  idMovie ; }
    public String getUsername() { return username ; }
    public int getNote() { return note ; }
    public String getAvis() { return avis ; }
    public String getPassword() { return mdp ; }

}
