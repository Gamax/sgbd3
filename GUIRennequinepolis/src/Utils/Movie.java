/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utils;

import java.util.ArrayList;

/**
 *
 * @author Steph
 */
public class Movie {
    private String id ;
    private String title ;
    private String title_original ;
    private String status ;
    private String release_date ;
    private String vote_averageTMDB ;
    private String vote_countTMDB ;
    private String vote_average ;
    private String vote_count ;
    private String certification ;
    private String runtime ;
    private String poster ;
    private ArrayList<String> acteurs ;
    private ArrayList<String> realisateurs ;
    private ArrayList<String> genres ;
    
    public Movie()
    {
        acteurs = new ArrayList<>() ;
        realisateurs = new ArrayList<>() ;
        genres = new ArrayList<>();
    }
    
    public Movie(String ID, String Title, String Title_original, String Status, String Date)
    {
        id = ID ;
        title = Title ;
        title_original = Title_original ;
        status = Status ;
        release_date = Date ;
        
        acteurs = new ArrayList<>() ;
        realisateurs = new ArrayList<>() ;
        genres = new ArrayList<>();
    }
    
    public Movie(String ID, String Title, String Title_original, String Status, String Date, String TMDBAvg, String TMDBCount, String Certif, String Run, String Poster,String VoteAVG, String NBVote )
    {
        id = ID ;
        title = Title ;
        title_original = Title_original ;
        status = Status ;
        release_date = Date ;
        vote_average = VoteAVG ;
        vote_count = NBVote ;
        certification = Certif ;
        runtime = Run ;
        poster = Poster ;
        vote_averageTMDB = TMDBAvg ;
        vote_countTMDB = TMDBCount ;
        acteurs = new ArrayList<>() ;
        realisateurs = new ArrayList<>() ;
        genres = new ArrayList<>();
        
    }
    
    public Movie(String ID, String Title, String Title_original, String Status, String Date, String TMDBAvg, String TMDBCount, String Certif, String Run, String Poster, String VoteAVG, String NBVote ,ArrayList<String> Acteurs, ArrayList<String> Realisateurs, ArrayList<String> Genres)
    {
        id = ID ;
        title = Title ;
        title_original = Title_original ;
        status = Status ;
        release_date = Date ;
        vote_average = VoteAVG ;
        vote_count = NBVote ;
        certification = Certif ;
        runtime = Run ;
        poster = Poster ;
        vote_averageTMDB = TMDBAvg ;
        vote_countTMDB = TMDBCount ;
        
        acteurs = new ArrayList<>(Acteurs) ;
        realisateurs = new ArrayList<>(Realisateurs);
        genres = new ArrayList<>(Genres);
    }
    
    
    public Movie(Movie original)
    {
        id = original.getID() ;
        title = original.getTitle() ;
        title_original = original.getOriginalTitle() ;
        status = original.getStatus() ;
        release_date = original.getDate() ;
        vote_average = original.getVoteAverage() ;
        vote_count = original.getVoteCount() ;
        certification = original.getCertification() ;
        runtime = original.getRuntime() ;
        poster = original.getPoster() ;
        vote_averageTMDB = original.getVoteAverageTMDB() ;
        vote_countTMDB = original.getVoteCountTMDB() ;
        
        acteurs = new ArrayList<>(original.getActeurs()) ;
        realisateurs = new ArrayList<>(original.getRealisateurs());
        genres = new ArrayList<>(original.getGenres());
        
    }
    
    public void setID (String value) { id = value ; }
    public void setTitle (String value) { title = value ; }
    public void setOriginalTitle (String value) { title_original = value ; }
    public void setStatus (String value) { status = value ; }
    public void setDate (String value) { release_date = value ; }
    public void setVoteAverage (String value) { vote_average = value ; }
    public void setVoteCount (String value) { vote_count = value ; }
    public void setCertification (String value) { certification = value ; }
    public void setRuntime (String value) { runtime = value ; }
    public void setPoster (String value) { poster = value ; }
    public void setActeurs(ArrayList<String> value) { acteurs = new ArrayList<>(value) ; }
    public void setRealisateurs(ArrayList<String> value) { realisateurs = new ArrayList<>(value) ; }
    public void setGenres(ArrayList<String> value) { genres = new ArrayList<>(value) ; }
    public void setVoteAverageTMDB (String value) { vote_averageTMDB = value ; }
    public void setVoteCountTMDB (String value) { vote_countTMDB = value ; }
    
    public void addActeur(String value) { acteurs.add(value); }
    public void addRealisateur(String value) { realisateurs.add(value); }
    public void addGenre(String value) { genres.add(value); }
    
    public String getID () { return id ; }
    public String getTitle () { return title ; }
    public String getOriginalTitle () { return title_original ; }
    public String getStatus () { return status ; }
    public String getDate () { return release_date ; }
    public String getVoteAverage () { return vote_average ; }
    public String getVoteCount () { return vote_count ; }
    public String getCertification () { return certification ; }
    public String getRuntime () { return runtime ; }
    public String getPoster () { return poster ; }
    public ArrayList<String> getActeurs () { return acteurs ; }
    public ArrayList<String> getRealisateurs() { return realisateurs ; }
    public ArrayList<String> getGenres() { return genres ; }
    public String getVoteAverageTMDB () { return vote_averageTMDB ; }
    public String getVoteCountTMDB () { return vote_countTMDB ; }
    
    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        
        if(id != null)
             str.append("Movie id : " + id + System.lineSeparator());
        if(title != null)
            str.append("Movie title : " + title + System.lineSeparator());
        if(title_original != null)
            str.append("Movie title_original : " + title_original + System.lineSeparator());
        if(release_date != null)
            str.append("Movie release_date : " + release_date + System.lineSeparator() );
        if(status != null)
            str.append("Movie status : " + status + System.lineSeparator());
        return str.toString() ;
    }
}