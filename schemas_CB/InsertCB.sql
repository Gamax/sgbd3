--INSERTION CERTIFICATION
INSERT INTO certification VALUES(1,'G','GENERAL AUDIENCES','All ages admitted','Nothing that would offend parents for viewing by children'); 
INSERT INTO certification VALUES(2,'PG','PARENTAL GUIDANCE SUGGESTED','Some material may not be suitable for children','Parents urged to give "parental guidance." May contain some material parents might not like for their young children');
INSERT INTO certification VALUES(3,'PG-13','PARENTS STRONGLY CAUTIONED','Some material may be inappropriate for children under 13','Parents are urged to be cautious. Some material may be inappropriate for pre-teenagers');
INSERT INTO certification VALUES(4,'R','RESTRICTED','Under 17 requires accompanying parent or adult guardian','Contains some adult material. Parents are urged to learn more about the ﬁlm before taking their young children with them');
INSERT INTO certification VALUES(5,'NC-17','ADULTS ONLY','No One 17 and Under Admitted','Clearly adult. Children are not admitted');
INSERT INTO certification VALUES(6,'IC','Invalid Certification','Invalid Certification','Invalid Certification');

--INSERTION STATUS
INSERT INTO STATUS values (1,'Post Production');
INSERT INTO STATUS values (2,'Rumored');
INSERT INTO STATUS values (3,'Released');
INSERT INTO STATUS values (4,'In Production');
INSERT INTO STATUS values (5,'Planned');
INSERT INTO STATUS values (6,'Canceled');
INSERT INTO STATUS values (7,'Invalid Status');

COMMIT;

