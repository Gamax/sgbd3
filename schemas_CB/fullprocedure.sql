drop table movie_genre cascade constraints ;
drop table movie_director cascade constraints;
drop table movie_actor cascade constraints;
drop table movie cascade constraints;
drop table artist cascade constraints;
drop table certification cascade constraints;
drop table status cascade constraints;
drop table genre cascade constraints;
drop table USER_ACCOUNT cascade constraints;
drop table USER_RATING cascade constraints;
create table artist (
  id   number(7),
  name varchar2(23 char) CONSTRAINT artist$NN NOT NULL,
  constraint artist$pk primary key (id)
);

-- https://en.wikipedia.org/wiki/Motion_Picture_Association_of_America_film_rating_system
-- https://filmratings.com/Content/Downloads/mpaa_ratings-poster-qr.pdf
-- https://www.uecmovies.com/movies/ratings
-- https://filmratings.com/RatingsGuide
create table certification (
  id          number(1),
  code        varchar2(5) CONSTRAINT certification$NN$code NOT NULL,
  name        varchar2(28) CONSTRAINT certification$NN$name NOT NULL,
  definition  varchar2(56) CONSTRAINT certification$NN$definition NOT NULL,
  description varchar2(122) CONSTRAINT certification$NN$description NOT NULL, -- default desc
  constraint cert$pk primary key (id)
);

create table status (
  id          number(1),
  name        varchar2(15) CONSTRAINT status$NN$name NOT NULL,
  constraint status$pk primary key (id)
);

create table genre (
  id   number(5),
  name varchar2(15) CONSTRAINT genre$NN$name NOT NULL,
  constraint genre$pk primary key (id)
);

create table movie (
  id             number(6),
  title          varchar2(60 char) CONSTRAINT movie$NN$title NOT NULL,
  original_title varchar2(60 char) CONSTRAINT movie$NN$O_title NOT NULL,
  status         number(1) CONSTRAINT movie$FK$status REFERENCES status(id),
  release_date   date,
  vote_average   number(3,1) CONSTRAINT movie$CK$value CHECK (vote_average BETWEEN 0 AND 10),
  vote_count     number(5), -->0
  certification  number(1) CONSTRAINT movie$FK$certification REFERENCES certification(id),
  runtime        number(3), -- minutes
  poster         blob,
  constraint movie$pk primary key (id)
);

create table movie_director (
  movie    number(6) CONSTRAINT m_d$FK$movie REFERENCES movie(id),
  director number(7) CONSTRAINT m_d$FK$artist REFERENCES artist(id),
  constraint m_d$pk primary key (movie, director)
);

create table movie_genre (
  movie number(6) CONSTRAINT m_g$FK$movie REFERENCES movie(id),
  genre number(5)CONSTRAINT m_g$FK$genre REFERENCES genre(id),
  constraint m_g$pk primary key (genre, movie)
  ) ;

create table movie_actor (
  movie number(6) CONSTRAINT m_a$FK$movie REFERENCES movie(id),
  actor number(7) CONSTRAINT m_a$FK$artist REFERENCES artist(id),
  constraint m_a$pk primary key (movie, actor)
);

CREATE TABLE user_account (
    username VARCHAR(20) CONSTRAINT u_a$PK PRIMARY KEY,
    password VARCHAR(20) CONSTRAINT u_a$NN$password NOT NULL
);

CREATE TABLE user_rating (
    userid VARCHAR(20) CONSTRAINT u_r$FK$account REFERENCES user_account(username),
    movie NUMBER(6) CONSTRAINT u_r$FK$movie REFERENCES movie(id),
    rate NUMBER(3) CONSTRAINT u_r$CK$value CHECK (rate BETWEEN 0 AND 10),
    description VARCHAR(500),
    date_modification DATE,
    flag number(1),
    CONSTRAINT u_r$PK PRIMARY KEY (userid, movie)
);

--INSERTION CERTIFICATION
INSERT INTO certification VALUES(1,'G','GENERAL AUDIENCES','All ages admitted','Nothing that would offend parents for viewing by children'); 
INSERT INTO certification VALUES(2,'PG','PARENTAL GUIDANCE SUGGESTED','Some material may not be suitable for children','Parents urged to give "parental guidance." May contain some material parents might not like for their young children');
INSERT INTO certification VALUES(3,'PG-13','PARENTS STRONGLY CAUTIONED','Some material may be inappropriate for children under 13','Parents are urged to be cautious. Some material may be inappropriate for pre-teenagers');
INSERT INTO certification VALUES(4,'R','RESTRICTED','Under 17 requires accompanying parent or adult guardian','Contains some adult material. Parents are urged to learn more about the ?lm before taking their young children with them');
INSERT INTO certification VALUES(5,'NC-17','ADULTS ONLY','No One 17 and Under Admitted','Clearly adult. Children are not admitted');
INSERT INTO certification VALUES(6,'IC','Invalid Certification','Invalid Certification','Invalid Certification');

--INSERTION STATUS
INSERT INTO STATUS values (1,'Post Production');
INSERT INTO STATUS values (2,'Rumored');
INSERT INTO STATUS values (3,'Released');
INSERT INTO STATUS values (4,'In Production');
INSERT INTO STATUS values (5,'Planned');
INSERT INTO STATUS values (6,'Canceled');
INSERT INTO STATUS values (7,'Invalid Status');

COMMIT;

begin
AlimPackage.ALIMTABLEMOVIES(AlimPackage.ID_array(594, 13, 35,497,568,591,594,640,862));
end;