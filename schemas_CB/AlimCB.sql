create or replace PACKAGE AlimPackage
AS
-- movies id list
  type ID_array is table of MOVIES_EXT.ID%TYPE;
  TYPE selected_movies_type IS TABLE OF movies_ext%ROWTYPE INDEX BY PLS_INTEGER;

  -- main functions
  PROCEDURE AlimFromRandom(NumberFilms INT);
  PROCEDURE AlimFromString(IdFilms STRING);
  PROCEDURE AlimTableMovies(IdMovies ID_ARRAY);

  -- utilities functions
  procedure AlimRow(movierow movies_ext%ROWTYPE);
  procedure ProcessingGenre(field VARCHAR2, id_movie MOVIE.ID%TYPE);
  procedure ProcessingActor(field VARCHAR2, id_movie MOVIE.ID%TYPE);
  procedure ProcessingDirector(field VARCHAR2, id_movie MOVIE.ID%TYPE);


END AlimPackage;

create or replace PACKAGE BODY AlimPackage AS

  --alim format function
  PROCEDURE AlimFromRandom(NumberFilms int) IS
    results id_array;

    cursor cResults is
      select ID
      from (select * from MOVIES_EXT order by DBMS_RANDOM.VALUE())
      where ROWNUM < NumberFilms + 1;

    BEGIN

      open cResults;
      fetch cResults bulk collect into results;
      close cResults;

      AlimTableMovies(results);

    END AlimFromRandom;

  PROCEDURE AlimFromString(IdFilms STRING) IS
    TabIDFilms id_ARRAY;
    BEGIN
      WITH DATA (s) AS (SELECT IdFilms FROM DUAL),
           SPLIT (s,
             stpos,
             endpos) AS (SELECT s, 1, instr(s, '$') FROM DATA
          UNION ALL
          SELECT s, endpos + 1, instr(s, '$', endpos + 1) FROM SPLIT WHERE endpos > 0),
           FINAL (stpos,
             endpos,
             s) AS (SELECT stpos, CASE endpos
                                    WHEN 0 THEN LENGTH(s)
                                    ELSE endpos - 1 END, SUBSTR(s, stpos, CASE endpos
                                                                            WHEN 0 THEN LENGTH(s) - stpos + 1
                                                                            ELSE endpos - stpos
          END
                               )
                    FROM SPLIT)
      SELECT s
          BULK COLLECT INTO TabIDFilms
      FROM FINAL
      ORDER BY stpos;
      AlimTableMovies(TabIDFilms);
    END AlimFromString;

  --main function

  PROCEDURE AlimTableMovies(IdMovies ID_ARRAY) IS
    selected_movies selected_movies_type;

    BEGIN

      select *
          bulk collect INTO selected_movies from MOVIES_EXT where ID IN (select * from table (IdMovies));

      FOR i IN selected_movies.first .. selected_movies.last LOOP
        AlimRow(selected_movies(i));
      END LOOP;

      EXCEPTION
      WHEN OTHERS
      THEN
        LOG_PKG.LOGERREUR('Error',true,'AlimTableMovies');
        RAISE;
    END AlimTableMovies;

  procedure AlimRow(movierow movies_ext%ROWTYPE) IS
    -- alim_error exception;
    -- PRAGMA EXCEPTION_INIT (alim_error, -20001);
    tempMovie MOVIE%rowtype;
    tempchar  varchar2(2000);
    BEGIN
      tempMovie.ID := movierow.ID;
      tempMovie.RELEASE_DATE := movierow.RELEASE_DATE;
      tempMovie.VOTE_AVERAGE := movierow.VOTE_AVERAGE;
      tempMovie.VOTE_COUNT := movierow.VOTE_COUNT;

      -- GESTION RUNTIME

      IF (movierow.RUNTIME is null)
      then
        tempMovie.RUNTIME := movierow.RUNTIME;
        LOG_PKG.LOGINFO('Runtime is null : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
      else
        if (movierow.RUNTIME > 999)
        then
          tempMovie.RUNTIME := 999;
          LOG_PKG.LOGINFO('Runtime is over 999 (' || movierow.RUNTIME ||') : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
        else
          tempMovie.RUNTIME := movierow.RUNTIME;
        end if;

      end if;

      -- GESTION TITRE

      tempchar := trim(movierow.TITLE);
      if (length(tempchar) >= 60)
      THEN
        LOG_PKG.LOGINFO('Title is too long : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
        tempMovie.TITLE := substrb(tempchar, 1, 60);
      else
        tempMovie.TITLE := tempchar;
      end if;

      -- GESTION TITRE ORIGINAL

      tempchar := trim(movierow.ORIGINAL_TITLE);
      if (length(tempchar) >= 60)
      THEN
        LOG_PKG.LOGINFO('Original Title is too long : ' || movierow.ORIGINAL_TITLE || '/' || movierow.ID,movierow.ID);
        tempMovie.ORIGINAL_TITLE := substrb(tempchar, 1, 60);
      else
        tempMovie.ORIGINAL_TITLE := tempchar;
      end if;

      -- GESTION BLOB

      BEGIN
        IF (movierow.poster_path IS NOT NULL)
        THEN
          tempMovie.POSTER := httpuritype.createuri(
              'http://image.tmdb.org/t/p/w185' || movierow.poster_path).getblob();
        END IF;
        EXCEPTION WHEN OTHERS
        THEN
          tempMovie.POSTER := empty_blob(); --todo
          LOG_PKG.LOGINFO('Cannot retrieve blob : '|| movierow.POSTER_PATH ||' : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
      END;

      -- GESTION STATUS
      begin
        SELECT ID
            INTO tempMovie.STATUS FROM STATUS WHERE movierow.STATUS = STATUS.NAME;
        exception when no_data_found
        then
          tempMovie.STATUS := 7;
          LOG_PKG.LOGINFO('Invalid Status : '|| movierow.STATUS ||' : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
      end;

      -- GESTION CERTIF
      begin
        SELECT ID
            INTO tempMovie.CERTIFICATION
        FROM CERTIFICATION
        WHERE movierow.CERTIFICATION = CERTIFICATION.CODE;
        exception when no_data_found
        then
          tempMovie.CERTIFICATION := 6;
          LOG_PKG.LOGINFO('Invalid Certification : '|| movierow.CERTIFICATION ||' : ' || movierow.TITLE || '/' || movierow.ID,movierow.ID);
      end;

      -- INSERTING

      SAVEPOINT insertingmovie;
      INSERT INTO MOVIE VALUES tempMovie;
      LOG_PKG.LOGINFO('Movie added sucessfully : '|| movierow.TITLE || '/' || movierow.ID,movierow.ID);

      -- GESTION ACTEUR
      ProcessingActor(movierow.ACTORS, movierow.ID);

      -- GESTION DIRECTEUR
      ProcessingDirector(movierow.DIRECTORS, movierow.ID);

      -- GESTION GENRE
      ProcessingGenre(movierow.GENRES, movierow.ID);

      COMMIT; -- peut être dehors

      EXCEPTION
      WHEN DUP_VAL_ON_INDEX -- gestion duplicates
      THEN
        LOG_PKG.LOGERREUR('Movie is already in database : '|| movierow.TITLE || ''/'' || movierow.ID,false,movierow.ID);
        ROLLBACK TO insertingmovie;

      WHEN OTHERS
      THEN
        LOG_PKG.LOGERREUR('Alim error on movie : '|| movierow.TITLE || ''/'' || movierow.ID,true,movierow.ID);
        ROLLBACK TO insertingmovie;


    END AlimRow;

  procedure ProcessingGenre(field VARCHAR2, id_movie MOVIE.ID%TYPE) is
    id_genre         INTEGER;
    name_genre       VARCHAR2(200);
    i                NUMBER := 1;
    insertSuccessful BOOLEAN := TRUE;
    BEGIN
      LOOP
        id_genre := TO_NUMBER(REGEXP_SUBSTR(field, '[^․‖]+', 1, i));
        name_genre := trim(REGEXP_SUBSTR(field, '[^․‖]+', 1, i + 1));
        EXIT WHEN id_genre IS NULL;

        IF (lengthb(name_genre) > 15)
        THEN
          name_genre := substrb(name_genre, 1, 15);
          LOG_PKG.LOGINFO('Genre name too long : ' || id_genre || ' : ' || id_movie,id_movie);
        END IF;

        -- check genre présent dans genre et ajout au besoin
        SAVEPOINT insertinggenre;
        BEGIN
          INSERT INTO genre (id, name) VALUES (id_genre, name_genre);
          LOG_PKG.LOGINFO('New genre inserted : ' || id_genre || ' : ' || id_movie,id_movie);
          EXCEPTION
          WHEN DUP_VAL_ON_INDEX
          THEN
            ROLLBACK TO insertinggenre;
          WHEN OTHERS
          THEN
            ROLLBACK TO insertinggenre;
            LOG_PKG.LOGERREUR('Processinggenre on insert : ' || id_genre || ' : ' || id_movie,true,id_movie);
            insertSuccessful := FALSE;
        END;

        -- création lien genre
        SAVEPOINT linkinggenre;
        IF (insertSuccessful = TRUE)
        THEN
          BEGIN
            INSERT INTO MOVIE_GENRE (movie, GENRE) VALUES (id_movie, id_genre);
            EXCEPTION
            WHEN OTHERS
            THEN
              ROLLBACK TO linkinggenre;
              LOG_PKG.LOGERREUR('Processinggenre on link : ' || id_genre || ' : ' || id_movie,true,id_movie);
          END;
        END IF;
        i := i + 2;
      END LOOP;

    end ProcessingGenre;

  procedure ProcessingActor(field VARCHAR2, id_movie MOVIE.ID%TYPE) is
    id_actor         INTEGER;
    name_actor       VARCHAR2(200);
    i                NUMBER := 1;
    insertSuccessful BOOLEAN := TRUE;
    BEGIN
      LOOP
        id_actor := TO_NUMBER(REGEXP_SUBSTR(field, '(‖|^)(\d+)․', 1, i, NULL,
                                            2)); -- match char/ position / occurence / match_parameter (not used) /
        name_actor := trim(REGEXP_SUBSTR(field, '․([^․‖]+)․', 1, i, NULL, 1));
        EXIT WHEN id_actor IS NULL;

        IF (lengthb(name_actor) > 23)
        THEN
          name_actor := substrb(name_actor, 1, 23);
          LOG_PKG.LOGINFO('Actor name too long : ' || id_actor || ' : ' || id_movie,id_movie);
        END IF;

        -- check actor présent dans artist et ajout au besoin
        SAVEPOINT insertingactor;
        BEGIN
          INSERT INTO artist (id, name) VALUES (id_actor, name_actor);
          EXCEPTION
          WHEN DUP_VAL_ON_INDEX
          THEN
            ROLLBACK TO insertingactor;
          WHEN OTHERS
          THEN
            ROLLBACK TO insertingactor;
            LOG_PKG.LOGERREUR('Processingactor on insert : ' || id_actor || ' : ' || id_movie,true,id_movie);
            insertSuccessful := FALSE;
        END;

        -- création lien actor
        SAVEPOINT linkingactor;
        IF (insertSuccessful = TRUE)
        THEN
          BEGIN
            INSERT INTO movie_actor (movie, actor) VALUES (id_movie, id_actor);
            EXCEPTION
            WHEN DUP_VAL_ON_INDEX
            THEN
              ROLLBACK TO linkingactor;
            WHEN OTHERS
            THEN
              ROLLBACK TO linkingactor;
              LOG_PKG.LOGERREUR('Processingactor on link : ' || id_actor || ' : ' || id_movie,true,id_movie);
          END;
        END IF;
        i := i + 1;
      END LOOP;
    END ProcessingActor;

  procedure ProcessingDirector(field VARCHAR2, id_movie MOVIE.ID%TYPE) is
    id_director      INTEGER;
    name_director    VARCHAR2(200);
    i                NUMBER := 1;
    insertSuccessful BOOLEAN := TRUE;
    BEGIN
      LOOP
        id_director := TO_NUMBER(REGEXP_SUBSTR(field, '(‖|^)(\d+)․', 1, i, NULL,
                                               2)); -- match char/ position / occurence / match_parameter (not used) / suboccurence
        name_director := trim(REGEXP_SUBSTR(field, '․([^․‖]+)', 1, i, NULL, 1));
        EXIT WHEN id_director IS NULL;

        IF (lengthb(name_director) > 23)
        THEN
          name_director := substrb(name_director, 1, 23);
          LOG_PKG.LOGINFO('Director name too long : ' || id_director || ' : ' || id_movie,id_movie);
        END IF;

        -- check director présent dans artist et ajout au besoin
        SAVEPOINT insertingdirector;
        BEGIN
          INSERT INTO artist (id, name) VALUES (id_director, name_director);
          EXCEPTION
          WHEN DUP_VAL_ON_INDEX
          THEN
            ROLLBACK TO insertingdirector;
          WHEN OTHERS
          THEN
            ROLLBACK TO insertingdirector;
            LOG_PKG.LOGERREUR('Processingactor on insert : ' || id_director || ' : ' || id_movie,true,id_movie);
            insertSuccessful := FALSE;
        END;

        -- création lien director
        SAVEPOINT linkingdirector;
        IF (insertSuccessful = TRUE)
        THEN
          BEGIN
            INSERT INTO movie_director (movie, director) VALUES (id_movie, id_director);
            EXCEPTION
            WHEN OTHERS
            THEN
              ROLLBACK TO linkingdirector;
              LOG_PKG.LOGERREUR('Processingactor on link : ' || id_director || ' : ' || id_movie,true,id_movie);
          END;
        END IF;
        i := i + 2;
      END LOOP;

    end ProcessingDirector;

END AlimPackage;



