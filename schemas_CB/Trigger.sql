CREATE OR REPLACE TRIGGER new_Vote BEFORE INSERT  ON user_rating
FOR EACH ROW
DECLARE
    y number(3,1);
    x number (3,1);
    vCount number(5);
BEGIN
        --SET (Y,:NEW.RATE);
        y := :NEW.RATE ;
        SELECT VOTE_COUNT INTO vCount FROM Movie WHERE id = :NEW.movie ;
        SELECT VOTE_AVERAGE INTO x FROM Movie WHERE id = :NEW.movie ;
        UPDATE movie SET vote_count = vote_count + 1
        WHERE id = :NEW.movie ; 
        UPDATE movie SET vote_average = ((x * vCount + y )/(vCount + 1)) 
        WHERE id = :NEW.movie ; 
END;
/

