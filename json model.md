## RechFilm

### Critères

#### Requete

````json
{
  "title": "Titre",
  "actors": [
    {
      "name": "Nom Acteur 1"
    },
    {
      "name": "Nom Acteur 2"
    },
    {
      "name": "Nom Acteur 3"
    }
  ],
  "directors": [
    {
      "name": "Nom Directeur 1"
    },
    {
      "name": "Nom Directeur 2"
    }
  ],
  "period": {
    "year": 2000,
    "time": "before"
  }
}
````

#### Réponse

````json
{
  "list": [
    {
      "id":4000,
      "title": "Titre",
      "original_title": "Titre Original",
      "release_date": "1995-10-09T23:00:00.000Z",
      "status": "Status"
    },
    {
      "id":4000,
      "title": "Titre",
      "original_title": "Titre Original",
      "release_date": "1995-10-09T23:00:00.000Z",
      "status": "Status"
    }
  ]
}
````

### Détails

#### Requete

via verbe GET

#### Réponse

````json
{
  "id":4000,
  "title": "Titre",
  "original_title": "Titre Original",
  "release_date": "2018-12-08T13:03:59.401Z",
  "status": "Status",
  "tmdb_votes_average": 3.4,
  "tmdb_votes_number": 350,
  "rqs_votes_average": 6.1,
  "rqs_votes_number": 15,
  "runtime": 950,
  "certification" : "certification",
  "poster": "9qzfd3f4s6q", -- encoding bytes
  "actors": [
    {
      "name": "Nom Acteur"
    },
    {
      "name": "Nom Acteur"
    },
    {
      "name": "Nom Acteur"
    }
  ],
  "directors": [
    {
      "name": "Nom Directeur"
    },
    {
      "name": "Nom Directeur"
    }
  ],
  "genres": [
    {
      "name": "Nom genre"
    },
    {
      "name": "Nom genre"
    }
  ]
}
````



### EvalCC

#### Requête

POST : evalCC.eval

```
{
    "action":"delete" -- add ou delete,
    "movie":"id_movie",
    "username":"username",
    "password":"password",
    "vote": 5, -- entier de 0 à 10
    "avis": "message"
}
```

#### Réponse

```
{
    "rqs_votes_average":5.5,
    "rqs_votes_number":535
}
```



#### Requête

POST : evalCC.avis

```
{
    "username":"username",
    "movie":"id_movie"
}
```

#### Réponse

```
{
    "vote":5, --entier de 0 à 10
    "avis":"message"
}
```

