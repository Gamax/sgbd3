create or replace package rechcc_pkg is
  procedure rechCritere(message varchar2);
  procedure rechID(id varchar2);
  procedure importCBtoCC(id_movie movie.id%type);
  function getAverageVotes(id integer) return number;
  function getNumberVotes(id integer) return integer;
end rechcc_pkg;
/