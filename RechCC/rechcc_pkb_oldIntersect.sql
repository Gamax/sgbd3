﻿create or replace package body rechcc_pkg is
  procedure rechCritere(message varchar2) is
    title_query varchar2(100);
    year        integer;
    time        integer; -- -1 : before / 0 : during / 1 : after / -2 : value is null
    actors_1    varchar2(1000);
    actors_2    varchar2(1000);
    actors_3    varchar2(1000);
    directors_1 varchar2(1000);
    directors_2 varchar2(1000);
    tempvarchar varchar2(1000);
    querycursor sys_refcursor;
    jsoncursor     sys_refcursor;
    -- for response
    id             movie.id%type;
    title          movie.title%type;
    original_title movie.original_title%type;
    status         movie.status%type;
    release_date   movie.release_date%type;
    begin


      -- PARSING JSON

      apex_json.parse(message);
      title_query := apex_json.get_varchar2('title');

      year := apex_json.get_number('period.year');

      tempvarchar := apex_json.get_varchar2('period.time');

      if (tempvarchar = 'before')
      then
        time := -1;
      end if;
      if (tempvarchar = 'during')
      then
        time := 0;
      end if;
      if (tempvarchar = 'after')
      then
        time := 1;
      end if;

      actors_1 := apex_json.get_varchar2('actors[1].name');
      actors_2 := apex_json.get_varchar2('actors[2].name');
      actors_3 := apex_json.get_varchar2('actors[3].name');

      directors_1 := apex_json.get_varchar2('directors[1].name');
      directors_2 := apex_json.get_varchar2('directors[2].name');

      -- END PARSING JSON

      -- TRAITEMENT

      open querycursor for
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      WHERE title_query IS NULL
         OR UPPER(TITLE) LIKE UPPER('%' || title_query || '%')
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      WHERE title_query IS NULL
         OR UPPER(ORIGINAL_TITLE) LIKE UPPER('%' || title_query || '%')
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where actors_1 is null
         or ID IN
            (select MOVIE
             from ARTIST@cblink
                    inner join movie_actor@cblink on ARTIST.ID = MOVIE_ACTOR.ACTOR
             where upper(NAME) = upper(actors_1))
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where actors_2 is null
         or ID IN
            (select MOVIE
             from ARTIST@cblink
                    inner join movie_actor@cblink on ARTIST.ID = MOVIE_ACTOR.ACTOR
             where upper(NAME) = upper(actors_2))
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where actors_3 is null
         or ID IN
            (select MOVIE
             from ARTIST@cblink
                    inner join movie_actor@cblink on ARTIST.ID = MOVIE_ACTOR.ACTOR
             where upper(NAME) = upper(actors_3))
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where directors_1 is null
         or ID IN
            (select MOVIE
             from ARTIST@cblink
                    inner join MOVIE_DIRECTOR@cblink on ARTIST.ID = MOVIE_DIRECTOR.DIRECTOR
             where upper(NAME) = upper(directors_1))
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where directors_2 is null
         or ID IN
            (select MOVIE
             from ARTIST@cblink
                    inner join MOVIE_DIRECTOR@cblink on ARTIST.ID = MOVIE_DIRECTOR.DIRECTOR
             where upper(NAME) = upper(directors_2))
      INTERSECT
      select ID, TITLE, ORIGINAL_TITLE, STATUS, RELEASE_DATE
      from MOVIE@cblink
      where year is null
         or time = -2
         or time = -1 and year > extract(year from MOVIE.RELEASE_DATE)
         or time = 0 and year = extract(year from MOVIE.RELEASE_DATE)
         or time = 1 and year < extract(year from MOVIE.RELEASE_DATE)
      order by ID;

      -- GENERATION JSON

      APEX_JSON.initialize_clob_output;
      APEX_JSON.open_object;
      APEX_JSON.open_array('list');

      LOOP
        FETCH querycursor into id, title, original_title, status, release_date;
        EXIT WHEN querycursor%notfound;

        select name into tempvarchar from STATUS@cblink where id = status;

        APEX_JSON.open_object;
        APEX_JSON.write('id', id);
        APEX_JSON.write('title', title);
        APEX_JSON.write('original_title', original_title);
        APEX_JSON.write('release_date', release_date);
        APEX_JSON.write('status', tempvarchar);
        APEX_JSON.close_object;

      END LOOP;
      close querycursor;

      APEX_JSON.close_array;
      APEX_JSON.close_object;

      -- CONSTRUCTION REPONSE

      owa_util.status_line(nstatus => 200, bclose_header => false);
      owa_util.mime_header(
          'text/html',
          bclose_header => true,
          ccharset => 'UTF-8'
      );

      htp.print(APEX_JSON.get_clob_output);

      EXCEPTION
      WHEN OTHERS
      THEN
        LOG_PKG.LOGERREUR('ERROR RechCCCritere', true);
    end rechCritere;

  procedure rechID(id varchar2) is
    id_int             integer;
    title              movie.title%type;
    original_title     movie.original_title%type;
    release_date       movie.release_date%type;
    status             varchar2(1000);
    tmdb_votes_average movie.vote_average%type;
    tmdb_votes_number  movie.vote_count%type;
    rqs_votes_average  integer;
    rqs_votes_number   integer;
    runtime            movie.runtime%type;
    certification   varchar2(1000);
    poster          clob;
    actor_cursor    sys_refcursor;
    director_cursor sys_refcursor;
    genre_cursor    sys_refcursor;
    name            artist.name%type;
    check_movie_cc  sys_refcursor;
    check_movie_cb  sys_refcursor;

    tempid          integer;


    begin

      id_int := to_number(id);

      open check_movie_cc for
      select id from movie where id_int = movie.id;
      fetch check_movie_cc into tempid;
      --close check_movie_cc;

      -- recherche id dans cc
      if (tempid is null)
      then --le film n'est pas dans cc, on check si il est dans cb
        open check_movie_cb for
        select id from movie@cblink mcb where id_int = mcb.id;
        fetch check_movie_cb into tempid;
        --close check_movie_cb;
        if (tempid is not null)
        then
          IMPORTCBTOCC(id_int);
        else
          -- le film n'est nulle part
          -- todo !
          DBMS_OUTPUT.PUT_LINE('Nulle part');
          return;
        end if;
      end if;

      --recup infos statiques

      select TITLE, ORIGINAL_TITLE, RELEASE_DATE, s2.NAME, VOTE_AVERAGE, VOTE_COUNT, RUNTIME, c2.NAME
          into rechID.title, rechID.original_title, rechID.release_date, rechID.status,
            tmdb_votes_average, tmdb_votes_number, rechID.runtime, rechID.certification
      from MOVIE m
             inner join CERTIFICATION C2 on m.CERTIFICATION = C2.ID
             inner join STATUS S2 on m.STATUS = S2.ID
      where m.id = id_int;

      --todo poster
      poster := 'Le poster lol';

      rqs_votes_average := getAverageVotes(id);

      rqs_votes_number := getNumberVotes(id);

      --recup actor

      open actor_cursor for
      select name from ARTIST a
                         inner join MOVIE_ACTOR ma on a.ID = ma.ACTOR where ma.MOVIE = id_int;

      --recup director

      open director_cursor for
      select name from ARTIST a
                         inner join MOVIE_DIRECTOR md on a.ID = md.DIRECTOR where md.MOVIE = id_int;

      --recup genre

      open genre_cursor for
      select name from GENRE g
                         inner join MOVIE_GENRE mg on g.ID = mg.GENRE where mg.MOVIE = id_int;

      -- génération json
      APEX_JSON.initialize_clob_output;
      APEX_JSON.open_object;
      APEX_JSON.write('title', title);
      APEX_JSON.write('original_title', original_title);
      APEX_JSON.write('release_date', release_date);
      APEX_JSON.write('status', status);
      APEX_JSON.write('tmdb_votes_average', tmdb_votes_average);
      APEX_JSON.write('tmdb_votes_number', tmdb_votes_number);
      APEX_JSON.write('rqs_votes_average', rqs_votes_average);
      APEX_JSON.write('rqs_votes_number', rqs_votes_number);
      APEX_JSON.write('runtime', runtime);
      APEX_JSON.write('certification', certification);
      APEX_JSON.write('poster', poster);

      APEX_JSON.open_array('actors');

      LOOP
        FETCH actor_cursor into name;
        EXIT WHEN actor_cursor%notfound;

        APEX_JSON.open_object;
        APEX_JSON.write('name', name);
        APEX_JSON.close_object;

      end loop;

      APEX_JSON.close_array;

      APEX_JSON.open_array('directors');

      LOOP
        FETCH director_cursor into name;
        EXIT WHEN director_cursor%notfound;

        APEX_JSON.open_object;
        APEX_JSON.write('name', name);
        APEX_JSON.close_object;

      end loop;

      APEX_JSON.close_array;

      APEX_JSON.open_array('genres');

      LOOP
        FETCH genre_cursor into name;
        EXIT WHEN genre_cursor%notfound;

        APEX_JSON.open_object;
        APEX_JSON.write('name', name);
        APEX_JSON.close_object;

      end loop;

      APEX_JSON.close_array;

      APEX_JSON.close_object;

      --CONSTRUCTION REPONSE

      owa_util.status_line(nstatus => 200, bclose_header => false);
      owa_util.mime_header(
          'text/html',
          bclose_header => true,
          ccharset => 'UTF-8'
      );

      htp.print(APEX_JSON.get_clob_output);

      EXCEPTION
      WHEN OTHERS
      THEN
        LOG_PKG.LOGERREUR('ERROR RechCCID', true);
    end rechID;

  procedure importCBtoCC(id_movie movie.id%type) is --on suppose que le film existe
    begin
      insert into MOVIE
          (select * from movie@cblink mcb where mcb.id = id_movie);

      -- Actor
      begin
        merge into ARTIST acc
        using (SELECT acb.ID as ID, acb.NAME as NAME
               from MOVIE_ACTOR@cblink macb
                      inner join ARTIST@cblink acb on macb.ACTOR = acb.ID
               where macb.MOVIE = id_movie) acb
        on (acc.ID = acb.ID)
        when not matched then
          insert (acc.ID, acc.NAME)
          values (acb.ID, acb.NAME);
        exception
        when others
        then
          LOG_PKG.LOGERREUR('Merge Error Actor: ' || id_movie, true, id_movie);
      end;

      begin
        insert into MOVIE_ACTOR select * from MOVIE_ACTOR@cblink where movie = id_movie;
        exception
        when dup_val_on_index
        then
          LOG_PKG.LOGERREUR('Imported Actor link error : ' || id_movie, true, id_movie);
      end;

      -- Director

      begin
        merge into ARTIST acc
        using (SELECT acb.ID as ID, acb.NAME as NAME
               from MOVIE_DIRECTOR@cblink mdcb
                      inner join ARTIST@cblink acb on mdcb.DIRECTOR = acb.ID
               where mdcb.MOVIE = id_movie) acb
        on (acc.ID = acb.ID)
        when not matched then
          insert (acc.ID, acc.NAME)
          values (acb.ID, acb.NAME);
        exception
        when others
        then
          LOG_PKG.LOGERREUR('Merge Error Director : ' || id_movie, true, id_movie);
      end;

      begin
        insert into MOVIE_DIRECTOR select * from MOVIE_DIRECTOR@cblink where movie = id_movie;
        exception
        when dup_val_on_index
        then
          LOG_PKG.LOGERREUR('Imported Director link error : ' || id_movie, true, id_movie);
      end;

      -- Genre

      begin
        merge into GENRE gcc
        using (SELECT gcb.ID as ID, gcb.NAME as NAME
               from MOVIE_GENRE@cblink mgcb
                      inner join GENRE@cblink gcb on mgcb.GENRE = gcb.ID
               where mgcb.MOVIE = id_movie) gcb
        on (gcc.ID = gcb.ID)
        when not matched then
          insert (gcc.ID, gcc.NAME)
          values (gcb.ID, gcb.NAME);
        exception
        when others
        then
          LOG_PKG.LOGERREUR('Merge Error Genre : ' || id_movie, true, id_movie);
      end;

      begin
        insert into MOVIE_GENRE select * from MOVIE_GENRE@cblink where movie = id_movie;
        exception
        when dup_val_on_index
        then
          LOG_PKG.LOGERREUR('Imported Genre link error : ' || id_movie, true, id_movie);
      end;

      --user_rating
      begin

        insert into USER_RATING select * from USER_RATING@cblink where id_movie = movie;
        exception
        when dup_val_on_index
        then
          LOG_PKG.LOGERREUR('Imported user_rating error : ' || id_movie, true, id_movie);

      end;

      LOG_PKG.LOGINFO('Imported movie : ' || id_movie, id_movie);
EXCEPTION
      WHEN OTHERS
      THEN
        LOG_PKG.LOGERREUR('ERROR importCBtoCC', true);
    end importCBtoCC;

  function getAverageVotes(id integer)
    return number is
    retour number(3,1);
    begin
      select avg(RATE) into retour from user_rating where movie = id;

      if(retour is null)
        then
        retour := 0;
      end if;

      return retour;
    end getAverageVotes;

  function getNumberVotes(id integer)
    return integer is
    retour integer;
    begin
      select count(RATE) into retour from user_rating where movie = id;
      return retour;
    end getNumberVotes;

end rechcc_pkg;