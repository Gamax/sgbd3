create or replace package body test_pkg is

procedure test(message varchar2 default null) is
  s varchar2(100);
begin
  owa_util.status_line(nstatus => 200, bclose_header => false);
  owa_util.mime_header (
    'text/html',
    bclose_header => true,
    ccharset => 'UTF-8'
  );
  if message is not null then
    htp.print('<p>Message : ' || message || '</p>');
  end if;
  select to_char(sysdate, 'DD/MM/YYYY HH24:MI:SS')
  into s from dual;
  htp.print('<p>' || s || '</p>');
end test;

end test_pkg;
/
show errors