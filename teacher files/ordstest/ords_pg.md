# Setup de ORDS pour PL/SQL Gateway uniquement

Ce document ne concerne que les étudiants de 3ème info-système.

## URLs

* Principale : https://www.oracle.com/database/technologies/appdev/rest.html
* Téléchargement : https://www.oracle.com/technetwork/developer-tools/rest-data-services/downloads/index.html
* Documentation : https://docs.oracle.com/en/database/oracle/oracle-rest-data-services/ qui nous amène à https://docs.oracle.com/en/database/oracle/oracle-rest-data-services/18.2/index.html
* Version : 18.2.0.r1831332 du 03/07/2018. C'est la même date de mise à jour que celle de SQL Developer.

## Téléchargement

On télécharge ORDS, ce qui nécessite des credentials Oracle (gratuit). Cela nous donne le fichier `ords-18.2.0.zip` que nous transférons dans le répertoire `/home/oracle/sgbd` de la VM.

La commande `unzip -l` nous permet de nous rendre compte qu'il faut décompresser ORDS dans un répertoire spécifique et pas dans `sgbd`.

```
[oracle@localhost ~]$ cd sgbd
[oracle@localhost sgbd]$ unzip -l ords-18.2.0.zip
[oracle@localhost sgbd]$ mkdir ords
[oracle@localhost sgbd]$ cd ords
[oracle@localhost ords]$ unzip ../ords-18.2.0.zip
[oracle@localhost ords]$ ls
docs  examples  index.html  ords.war  params
[oracle@localhost ords]$ ls params/
ords_params.properties
```

Le fichier `ords.war` sera modifié par l'installation que nous allons faire. Cela signifie que si on souhaite recommencer la configuration, il est nécessaire de repartir de zéro avec un `ords.war` original et donc effacer le répertoire et re-décompresser l'archive.

## Installation

Comme répertoire de configuration pour ORDS, nous allons indiquer `/home/oracle/sgbd` car le processus d'installation installera la configuration dans un sous-répertoire `ords` dans le répertoire mentionné. Cela signifie que ça permettra de placer la configuration dans notre répertoire `ords` sans créer une arborescence inutilement profonde.

Les choix réalisés lors de l'installation interactive (questions / réponses) sont mémorisés dans le fichier `params/ords_params.properties`. Nous allons réaliser une installation avancée de manière à pouvoir personnaliser un certain nombre de choses. Les mots de passe indiqués sont toujours `oracle`.

```
[oracle@localhost ords]$ java -jar ords.war install advanced
This Oracle REST Data Services instance has not yet been configured.
Please complete the following prompts

Enter the location to store configuration data:/home/oracle/sgbd
Enter the name of the database server [localhost]:
Enter the database listen port [1521]:
Enter 1 to specify the database service name, or 2 to specify the database SID [1]:
Enter the database service name:orcl
Enter 1 if you want to verify/install Oracle REST Data Services schema or 2 to skip this step [1]:2
Enter 1 if you want to use PL/SQL Gateway or 2 to skip this step.
If using Oracle Application Express or migrating from mod_plsql then you must enter 1 [1]:1
Enter the PL/SQL Gateway database user name [APEX_PUBLIC_USER]:cc
Enter the database password for cc:
Confirm password:
Enter 1 to specify passwords for Application Express RESTful Services database users (APEX_LISTENER, APEX_REST_PUBLIC_USER) or 2 to skip this step [1]:2
Sep 29, 2018 3:36:41 PM
INFO: Updated configurations: defaults, apex
Enter 1 if you wish to start in standalone mode or 2 to exit [1]:2
```

L'installation de ORDS est terminée mais nous devons finaliser la configuration avant de pouvoir l'utiliser. On peut voir que le fichier `apex.xml` a été créé dans le répertoire `conf` et que le fichier `params/ords_params.properties` a été mis à jour.

Le fichier `apex.xml` contient les informations nécessaires pour accéder à la base de données en tant que `CB`, ce qui sera utilisé par le PL/SQL Gateway pour invoquer nos procédures. Attention, le terme `apex` est trompeur puisqu'on n'utilise pas APEX ([Oracle Application Express](https://apex.oracle.com/)). Nous allons donc le renommer en `cc.xml`.

```
[oracle@localhost conf]$ pwd
/home/oracle/sgbd/ords/conf
[oracle@localhost conf]$ mv apex.xml cc.xml
[oracle@localhost conf]$ ls
cc.xml
```

On crée ensuite le mapping des URLs.

```
[oracle@localhost ords]$ java -jar ords.war map-url --type base-path /cc cc
Sep 29, 2018 3:40:34 PM
INFO: Creating new mapping from: [base-path,/cc] to map to: [cc, null, null]
```

On peut regarder le contenu du fichier nouvellement créé appelé `url-mapping.xml`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<pool-config xmlns="http://xmlns.oracle.com/apex/pool-config">
 <pool name="cc" base-path="/cc" updated="2018-09-29T13:40:34.827Z"/>
</pool-config>
```

Dans le fichier `defaults.xml`, on s'arrange pour avoir les éléments `entry` ci-dessous. Ils permettent dans l'ordre de :
1. Eviter de devoir utiliser le protocole HTTPS : `<entry key="security.verifySSL">false</entry>`.
1. Disposer de debug tracing ajouté dans la sortie de log : `<entry key="debug.debugger">true</entry>`.
1. Disposer d'un message détaillé en cas d'erreur avec la stack trace : `<entry key="debug.printDebugToScreen">true</entry>`.

Plus précisément, l'élément `entry` relatif à la vérification de SSL doit être ajouté comme dernier enfant de l'élément racine. Les deux autres éléments existent déjà et il faut changer leur contenu de `false` à `true`.

```xml
<properties>
  ...
  <entry key="debug.debugger">true</entry>
  <entry key="debug.printDebugToScreen">true</entry>
  ...
  <entry key="security.verifySSL">false</entry>
</properties>
```

## Test PL/SQL Gateway

On peut maintenant tester l'installation du PL/SQL Gateway en mode standalone, c'est-à-dire qu'on n'intègre pas notre `ords.war` dans un serveur applicatif de notre choix comme Tomcat mais plutôt qu'on utilise le serveur intégré à ORDS qui est Jetty.

On utilise HTTP pour éviter le setup supplémentaire dû à HTTPS même s'il vaut clairement mieux utiliser HTTPS en production. Le port 8080 étant déjà utilisé nous choisissons le 9082 pour notre test. Les informations renvoyées par l'exécution de la commande ont été élaguées. Les informations indiquées interactivement seront sauvegardées.

On ajoute une règle de redirection des ports dans Virtual Box.
| Name | Protocol | Host IP | Host Port | Guest IP | Guest Port |
| ---- | -------- | ------- | --------- | -------- | ---------- |
| ords | TCP | 127.0.0.1 | 9082 | | 9082 |

```
[oracle@localhost ords]$ java -jar ords.war standalone
Enter 1 if using HTTP or 2 if using HTTPS [1]:
Enter the HTTP port [8080]:9082
...
INFO: HTTP and HTTP/2 cleartext listening on port: 9082
...
INFO: Disabling document root because the specified folder does not exist: /home/oracle/sgbd/ords/standalone/doc_root
...
INFO: Creating Pool:|cc||
INFO: Configuration properties for: |cc||
db.username=cc
...
```

Dans le schéma `CC`, on crée le package `test_pkg` défini par les fichiers SQL `test_pks.sql` et `test_pkb.sql` dans(https://bitbucket.org/lkuty/sgbd/src/default/section/3s/).

On lance un navigateur sur la machine hôte et on teste l'URL suivante : <http://localhost:9082/ords/cc/test_pkg.test>.
