create or replace package test_pkg is

procedure test(message varchar2 default null);

end test_pkg;
/
show errors