select
  json_objectagg(
    'test' value 'éééé'
    returning clob
    -- returning varchar2
  ) v
from dual;

-- 12.2 KO !
/*
{"test":"éééé"ATS
}
*/

-- 18.3 OK
/*
{"test":"éééé"}
*/

declare
  c clob;
begin
  select
    json_objectagg(
      'test' value 'éééé'
      returning clob
      -- returning varchar2
    ) v
  into c
  from dual;
  dbms_output.put_line(c);
end;
/

-- 12.2 KO !
/*
{"test":"éééé"*蝉
*/

-- 18.3 OK
/*
{"test":"éééé"}
*/
