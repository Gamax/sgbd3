create or replace package body json_pkg is

---------------------------------------------------------
-- test1
-- Display parameters sent with GET/POST
-- http://localhost:9082/ords/cc/json_pkg.test1
---------------------------------------------------------
procedure test1(
  title          varchar2 default null,
  original_title varchar2 default null,
  actors         varchar2 default null
) is
  s varchar2(100);
  function li(name varchar2, value varchar2) return varchar2 is
  begin
    return '<li>' || name || ' : ' || case when value is null then 'NULL' else value end || '</li>';
  end;
begin
  owa_util.status_line(nstatus => 200, bclose_header => false);
  owa_util.mime_header (
    'text/html',
    bclose_header => true,
    ccharset => 'UTF-8'
  );
  htp.print('<ul>');
  htp.print(li('title', title));
  htp.print(li('original_title', original_title));
  htp.print(li('actors', actors));
  htp.print('</ul>');
end test1;

function load_json_from_file(filename varchar2)
return clob is
  file bfile;
  c clob;
  dest_offset integer := 1;
  src_offset integer := 1;
  lang_context integer := 0;
  warning integer;
begin
  file := bfilename('MYDIR', filename);
  dbms_lob.open (file);
  dbms_lob.createtemporary (c, cache => false, dur => dbms_lob.session);
  dbms_lob.loadclobfromfile(
    c,
    file,
    dbms_lob.lobmaxsize, dest_offset, src_offset, 0, lang_context, warning);
  dbms_lob.close (file);
  return c;
end load_json_from_file;

---------------------------------------------------------
-- test2
-- Send JSON file explicitly with a CLOB
-- Test with tom_hanks_short.json and tom_hanks_long.json
-- http://localhost:9082/ords/cc/json_pkg.test2
-- http://localhost:9082/ords/cc/json_pkg.test2?filename=tom_hanks_short.json
-- http://localhost:9082/ords/cc/json_pkg.test2?filename=tom_hanks_long.json
---------------------------------------------------------
procedure test2(filename varchar2 default null) is
  c clob;
begin
  if filename is null then
    htb.send(
      v => '{"message" : "Hello World !"}',
      content_type => 'application/json'
    );
    return;
  end if;
  c := load_json_from_file(filename);
  htb.send(c, 'application/json');
  dbms_lob.freetemporary(c);
end test2;

---------------------------------------------------------
-- test3
-- Send JSON file using wpg_docload
-- Test with tom_hanks_short.json and tom_hanks_long.json
-- http://localhost:9082/ords/cc/json_pkg.test3
-- http://localhost:9082/ords/cc/json_pkg.test3?filename=tom_hanks_short.json
-- http://localhost:9082/ords/cc/json_pkg.test3?filename=tom_hanks_long.json
---------------------------------------------------------
procedure test3(filename varchar2 default null) is
  file bfile;
begin
  if filename is null then
    htb.send(
      v => '{"message" : "Hello World !"}',
      content_type => 'application/json'
    );
    return;
  end if;
  file := bfilename('MYDIR', filename);
  wpg_docload.download_file(file);
end test3;

/*
https://docs.oracle.com/en/database/oracle/oracle-database/12.2/readm/json-known-bugs.html
Bug 24693010
Workaround:
If possible, limit use of JSON_OBJECTAGG to VARCHAR2 or JSON_ARRAYAGG to VARCHAR2
results and avoid CLOB outputs. Use CLOB outputs only when using single-byte
character sets.
*/
---------------------------------------------------------
-- test4
-- Send JSON data with SQL/JSON functions
-- Test with provinces.sql
-- https://docs.oracle.com/en/database/oracle/oracle-database/12.2/adjsn/generation.html
-- http://localhost:9082/ords/cc/json_pkg.test4
---------------------------------------------------------
procedure test4 is
  c clob;
  v varchar2(4000);
begin
  select
  json_arrayagg(
      json_object(
          'id' value p.id,
          'nom_fr' value translate(p.nom_fr, 'è', 'e'),
          'nom_nl' value p.nom_nl,
          'chef_lieu' value translate(v2.nom_fr, 'è', 'e'),
          'population' value p.population,
          'superficie' value p.superficie,
          'villes' value json_arrayagg(
            json_object(
                'id' value v.id,
                'nom_fr' value translate(v.nom_fr, 'è', 'e'),
                'nom_nl' value v.nom_nl
            )
          )
      )
      order by p.id
      returning clob
      -- returning varchar2
  ) v
  into c
  -- into v
  from Province p, Ville v, Ville v2
  where v.province = p.id
    and p.chef_lieu = v2.id
  group by p.id, p.nom_fr, p.nom_nl, v2.nom_fr, p.population, p.superficie;

  -- htb.send(v => v, content_type => 'application/json');
  htb.send(c, 'application/json; charset=utf-8');
end test4;

---------------------------------------------------------
-- test5
-- Send JSON data with apex_json and cursor expressions
-- Test with provinces.sql
-- https://docs.oracle.com/cd/E59726_01/doc.50/e39149/apex_json.htm
-- http://localhost:9082/ords/cc/json_pkg.test5
---------------------------------------------------------
procedure test5 is
  c sys_refcursor;
begin
  open c for
  select
    p.id "id",
    p.nom_fr "nom_fr",
    p.nom_nl "nom_nl",
    v.nom_fr "chef_lieu",
    p.population "population",
    p.superficie "superficie",
    cursor(
      select id "id", nom_fr "nom_fr", nom_nl "nom_nl"
      from ville
      where province = p.id
    ) "villes"
  from Province p, Ville v
  where p.chef_lieu = v.id
  order by p.id;

  -- There is no way to stop the response from being sent
  -- in case of an error. Cfr. procedure test9
  apex_json.initialize_output(p_http_header => false);
  sys.owa_util.mime_header('application/json; charset=utf-8', false);
  sys.owa_util.status_line(200);
  sys.owa_util.http_header_close;
  apex_json.write(c);
  apex_json.flush;
end test5;

---------------------------------------------------------
-- json2buffer (private)
-- Sends a JSON doc as HTML to the HTB buffer
---------------------------------------------------------
procedure json2buffer(json json_element_t) is
  keys json_key_list;
  jo json_object_t;
  ja json_array_t;
begin
  if json.is_object then
    jo := treat(json as json_object_t);
    keys := jo.get_keys;
    htb.append_nl('<ul>');
    for i in 1..keys.count loop
       htb.append('<li>' || keys(i) || ' ➔ ');
       json2buffer(jo.get(keys(i)));
       htb.append_nl('</li>');
    end loop;
    htb.append_nl('</ul>');
  elsif json.is_array then
    ja := treat(json as json_array_t);
    htb.append_nl('<ul>');
    for i in 0..json.get_size - 1 loop
      htb.append('<li>');
      json2buffer(ja.get(i));
      htb.append_nl('</li>');
    end loop;
    htb.append_nl('</ul>');
  else
    htb.append(json.to_string);
  end if;
end json2buffer;

/*
{
  "actors": ["hardy", "theron"],
  "year": ["<", 2010]
}
http://localhost:9082/ords/cc/json_pkg.test6?query=%7B%22actors%22:[%22hardy%22,%22theron%22],%22year%22:[%22%3C%22,2010]%7D
*/
---------------------------------------------------------
-- test6
-- Display JSON data as HTML with PL/SQL Object Types for JSON
-- https://docs.oracle.com/en/database/oracle/oracle-database/12.2/adjsn/pl-sql-object-types-for-json.html
-- https://docs.oracle.com/en/database/oracle/oracle-database/12.2/arpls/json-types.html
---------------------------------------------------------
procedure test6(query clob) is
  json json_element_t;
begin
  json := json_element_t.parse(query);
  json2buffer(json);
  htb.send;
end test6;

---------------------------------------------------------
-- test7
-- Display JSON file as HTML with PL/SQL Object Types for JSON
---------------------------------------------------------
procedure test7(filename varchar2) is
  json json_element_t;
  c clob;
begin
  c := load_json_from_file(filename);
  json := json_element_t.parse(c);
  json2buffer(json);
  htb.send;
  dbms_lob.freetemporary(c);
end test7;

---------------------------------------------------------
-- test8
-- Error handling
-- Status Codes : https://tools.ietf.org/html/rfc7231#section-6
---------------------------------------------------------
procedure test8 is
begin
  htb.append_nl('<html><body>');
  htb.append_nl('<ul>');
  htb.append_nl('<li>Item 1</li>');
  raise zero_divide;
  htb.append_nl('<li>Item 2</li>');
  htb.append_nl('</ul>');
  htb.append_nl('</body></html>');
exception when others then
  Log_pkg.LogErreur;
  htb.reset;
  htb.append_nl('<html><body>');
  htb.append_nl('<p>SQLERRM : ' || htf.escape_sc(sqlerrm) || '</p>');
  htb.append_nl('</body></html>');
  htb.send(status_code => 500);
end test8;

---------------------------------------------------------
-- test9
-- Send JSON data with apex_json and cursor expressions
-- Test with provinces.sql
-- https://docs.oracle.com/cd/E59726_01/doc.50/e39149/apex_json.htm
-- http://localhost:9082/ords/cc/json_pkg.test9
---------------------------------------------------------
procedure test9 is
  c sys_refcursor;
begin
  open c for
  select
    p.id "id",
    p.nom_fr "nom_fr",
    p.nom_nl "nom_nl",
    v.nom_fr "chef_lieu",
    p.population "population",
    p.superficie "superficie",
    cursor(
      select id "id", nom_fr "nom_fr", nom_nl "nom_nl"
      from ville
      where province = p.id
    ) "villes"
  from Province p, Ville v
  where p.chef_lieu = v.id
  order by p.id;

  apex_json.initialize_clob_output;
  apex_json.write(c);
  raise zero_divide;
  htb.send(apex_json.get_clob_output, 'application/json; charset=utf-8');
  apex_json.free_output;
exception when others then
  Log_pkg.LogErreur;
  htb.reset;
  htb.append_nl('<html><body>');
  htb.append_nl('<p>SQLERRM : ' || htf.escape_sc(sqlerrm) || '</p>');
  htb.append_nl('</body></html>');
  htb.send(status_code => 500);
end test9;

end json_pkg;
/
show errors
