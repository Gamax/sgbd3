drop table province cascade constraints;
drop table ville cascade constraints;

create table ville (
  id       integer,
  nom_fr   varchar2(100),
  nom_nl   varchar2(100),
  province integer,
  constraint ville$pk primary key (id),
  constraint ville$nom_fr$nn check (nom_fr is not null)
);

create table province (
    id         integer,
    nom_fr     varchar2(100),
    nom_nl     varchar2(100),
    chef_lieu  integer,
    population integer,
    -- superficie en km^2
    superficie integer,
    constraint province$pk primary key (id),
    constraint province$chef_lieu$ville$fk foreign key (chef_lieu) references ville (id),
    constraint province$nom_fr$nn check (nom_fr is not null),
    constraint province$nom_fr$un unique(nom_fr),
    constraint province$nom_nl$un unique(nom_nl),
    constraint province$chef_lieu$nn check (chef_lieu is not null),
    constraint province$population$positif check(population > 0),
    constraint province$superficie$positif check(superficie > 0)
);

insert into ville values (1,  'Waremme',  'Borgworm',  8);
insert into ville values (2,  'Aywaille',  null,       8);
insert into ville values (3,  'Anvers',   'Antwerpen', 1);
insert into ville values (4,  'Hasselt',  'Hasselt',   2);
insert into ville values (5,  'Gand',     'Gent',      3);
insert into ville values (6,  'Louvain',  'Leuven',    4);
insert into ville values (7,  'Bruges',   'Brugge',    5);
insert into ville values (8,  'Wavre',     null,       6);
insert into ville values (9,  'Mons',      null,       7);
insert into ville values (10, 'Liège',    'Luik',      8);
insert into ville values (11, 'Arlon',     null,       9);
insert into ville values (12, 'Namur',    'Namen',    10);

insert into province values (1, 'Anvers',              'Antwerpen',       3, 1813282,  2867);
insert into province values (2, 'Limbourg',            'Limburg',         4, 860204,   2422);
insert into province values (3, 'Flandre Orientale',   'Oost-Vlaanderen', 5, 1477346,  2982);
insert into province values (4, 'Brabant Flamand',     'Vlaams-Brabant',  6, 1114299,  2106);
insert into province values (5, 'Flandre Occidentale', 'West-Vlaanderen', 7, 11788996, 3144);
insert into province values (6, 'Brabant Wallon',      'Walloon-Brabant', 8, 393700,   1091);
insert into province values (7, 'Hainaut',              null,             9, 1335360,  3786);
insert into province values (8, 'Liège',               'Luik',           10, 1094791,  3862);
insert into province values (9, 'Luxembourg',          'Luxemburg',      11, 278748,   4440);
insert into province values (10, 'Namur',              'Namen',          12, 487145,   3666);

alter table ville
add constraint ville$province$fk foreign key (province)
references province(id);

commit;
