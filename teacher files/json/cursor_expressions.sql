-- https://docs.oracle.com/en/database/oracle/oracle-database/12.2/sqlrf/CURSOR-Expressions.html

-- HR schema
select
  department_name,
  cursor(
    select salary, commission_pct
    from employees e
    where e.department_id = d.department_id
  ) cursor_expr
from departments d
order by department_name;

-- Provinces
select
  p.id "id",
  p.nom_fr "nom_fr",
  p.nom_nl "nom_bl",
  v.nom_fr "chef_lieu",
  p.population "population",
  p.superficie "superficie",
  cursor(
    select id "id", nom_fr "nom_fr", nom_nl "nom_nl"
    from ville
    where province = p.id
  ) "villes"
  from Province p, Ville v
  where p.chef_lieu = v.id
  order by p.id;
