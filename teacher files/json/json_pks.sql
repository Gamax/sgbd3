create or replace package json_pkg is

procedure test1(
  title          varchar2 default null,
  original_title varchar2 default null,
  actors         varchar2 default null
);

procedure test2(filename varchar2 default null);
procedure test3(filename varchar2 default null);

procedure test4;
procedure test5;

procedure test6(query clob);
procedure test7(filename varchar2);

procedure test8;
procedure test9;

end json_pkg;
/
show errors
