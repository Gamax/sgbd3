drop table LogErreurs;
drop table LogInfos;

create table LogErreurs (
  id              integer generated always as identity,
  tracking        varchar2(128),
  who             varchar2(128),
  datetime        timestamp(6),
  message         varchar2(4000),
  code            integer,
  errm            varchar2(512),
  error_backtrace varchar2(2000),
  error_stack     varchar2(2000),
  constraint LogErreurs$PK primary key (id),
  constraint LogErreurs$who$NN check (who is not null),
  constraint LogErreurs$datetime$NN check (datetime is not null),
  constraint LogErreurs$code$NN
    check (code is not null or message is not null),
  constraint LogErreurs$errm$NN
    check (errm is not null or message is not null),
  constraint LogErreurs$error_backtrace$NN
    check (error_backtrace is not null or message is not null),
  constraint LogErreurs$error_stack$NN
    check (error_stack is not null or message is not null)
);

create table LogInfos (
  id       integer generated always as identity,
  tracking varchar2(128),
  who      varchar2(128),
  datetime timestamp(6),
  message  varchar2(4000),
  constraint LogInfos$PK primary key (id),
  constraint LogInfos$who$NN check (who is not null),
  constraint LogInfos$datetime$NN check (datetime is not null),
  constraint LogInfos$message$NN check (message is not null)
);