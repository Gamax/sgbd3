create or replace package purgeCC_pkg is
    delay constant INTERVAL DAY(3) TO SECOND(0) := INTERVAL '5' MINUTE; -- INTERVAL '5:30' MINUTE TO SECOND
    procedure purge;
  procedure deletemovie(id movie.id%type);

  end purgeCC_pkg;

