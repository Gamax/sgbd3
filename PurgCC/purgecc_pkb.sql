create or replace package body purgeCC_pkg is
  procedure purge is

    cursor film_cursor
    is
      select id
      from MOVIE
      where ID in (select MOVIE from USER_RATING where DATE_MODIFICATION < current_date - delay);
    cursor film_without_rating
    is
      select id
      from MOVIE m
      where 0 = (select count(RATE) from user_rating where movie = M.ID);

    id movie.ID%type;

    begin
      open film_cursor;
      LOOP
        FETCH film_cursor into id;
        EXIT WHEN film_cursor%notfound;

        --check réplication

        -- réplication user ayant noté le film

        begin
          merge into USER_ACCOUNT@cblink uacb
          using (select USERNAME, PASSWORD
                 from USER_ACCOUNT
                        inner join USER_RATING RATING on USER_ACCOUNT.USERNAME = RATING.USERID
                 where id = MOVIE) uacc
          on (uacc.USERNAME = uacb.USERNAME)
          when not matched then
            insert (uacb.USERNAME, uacb.PASSWORD)
            values (uacc.USERNAME, uacc.PASSWORD);
          exception
          when others
          then
            LOG_PKG.LOGERREUR('purgeCCJob error on user_account merge', true);
        end;

        -- réplic

        begin
          merge into USER_RATING@cblink urcb
          using (select * from USER_RATING where MOVIE = id) urcc
          on (urcc.USERID = urcb.USERID AND urcc.MOVIE = urcb.movie)
          when not matched then
            insert (urcb.USERID, urcb.MOVIE, urcb.RATE, urcb.DESCRIPTION, urcb.DATE_MODIFICATION)
            values (urcc.USERID, urcc.MOVIE, urcc.RATE, urcc.DESCRIPTION, urcc.DATE_MODIFICATION);
          exception
          when others
          then
            LOG_PKG.LOGERREUR('purgeCCJob error on user_rating merge', true);
        end;

        deletemovie(id);

      end loop;
      close film_cursor;

      -- on supprime aussi tous les films sans notes
      open film_without_rating;
      LOOP
        FETCH film_without_rating into id;
        EXIT WHEN film_without_rating%notfound;

        deletemovie(id);

      end loop;
      close film_without_rating;

      COMMIT;
      exception
      when others
      then
        LOG_PKG.LOGERREUR('purgeCCJob error', true);
    end purge;

  procedure deletemovie(id movie.id%type) is
    begin
      -- delete lien acteur
      delete from MOVIE_ACTOR where MOVIE = deletemovie.id;
      delete from MOVIE_DIRECTOR where MOVIE = deletemovie.id;
      delete from MOVIE_GENRE where MOVIE = deletemovie.id;

      delete from ARTIST where id not in (select actor from MOVIE_ACTOR) and id not in (select DIRECTOR from MOVIE_DIRECTOR);
      delete from GENRE where id not in (select  genre from MOVIE_GENRE);

      --set le flag
      update USER_RATING set FLAG = 1 where MOVIE = deletemovie.id;

      --delete user_rating
      delete from USER_RATING where MOVIE = deletemovie.id;

      --delete movie
      delete from MOVIE where ID = deletemovie.id;
    end deletemovie;

end purgeCC_pkg;
/