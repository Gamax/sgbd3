begin
  dbms_scheduler.create_job(
      job_name        => 'job_purge',
      job_type        => 'STORED_PROCEDURE',
      job_action      => 'purgeCC_pkg.purge',
      start_date      => NULL,
      repeat_interval => 'FREQ=WEEKLY;BYDAY=MON;byhour=2;byminute=0;bysecond=0',
      auto_drop       =>  FALSE,
      comments        => 'Purge old movie',
      enabled         => FALSE);
  dbms_scheduler.enable('job_purge');
  commit;
end;
/

begin
  dbms_scheduler.run_job('job_purge');
end;
begin
  dbms_scheduler.drop_job(job_name => 'job_purge');
end;

  select * from user_scheduler_job_log order by log_date desc;
select * from (select * from user_scheduler_job_log order by log_date desc) where rownum < 3;
  select * from user_scheduler_job_run_details order by log_date desc;
select * from (select * from user_scheduler_job_run_details order by log_date desc) where rownum = 1;