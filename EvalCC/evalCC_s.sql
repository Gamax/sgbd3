create or replace package evalCC IS
    procedure eval(message varchar2);
    procedure avis(message varchar2);
    procedure evalCCJob;
   
END evalCC;
/