create or replace trigger evalCC_trigger
  after insert or update or delete
  on user_rating
  for each row
  declare
    existbool integer;
  begin

    --check is le user existe dans cb

    select case
             when exists(select USERNAME from USER_ACCOUNT@cblink where :new.USERID = username
                                                                     or :old.USERID = username)
                     then 1
             else 0
               end
        into existbool
    from dual;

    if existbool = 0
    then
      return; -- l'user n'existe pas on ne fait rien
    end if;

    if inserting
    then
      -- check si import
      if (:new.FLAG = 1)
      then
        return; -- c'est une insertion et le tuple existe déjà en cb, c'est donc lors d'un import
      end if;

      --réplication
      insert into USER_RATING@cblink
      values (:new.USERID, :new.MOVIE, :new.RATE, :new.DESCRIPTION, :new.DATE_MODIFICATION,0);

    end if;

    if updating
    then
      -- modif du tuple en cb
      update USER_RATING@cblink
      set RATE        = :new.RATE,
          DESCRIPTION = :new.DESCRIPTION
      where :new.USERID = USERID
        and :new.MOVIE = MOVIE;

    end if;
    if deleting
    then
      -- delete tuple en cb
      if (:old.FLAG = 0)
      then
        delete from USER_RATING@cblink where :old.USERID = USERID
                                         and :old.MOVIE = MOVIE;
      end if;
    end if;

    EXCEPTION
    WHEN OTHERS
    THEN
      LOG_PKG.LOGERREUR('ERROR TrigevalCC', true);
  end;
  /
alter trigger evalCC_trigger enable;