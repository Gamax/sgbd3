begin
  dbms_scheduler.create_job(
      job_name        => 'job_eval',
      job_type        => 'STORED_PROCEDURE',
      job_action      => 'evalCC.evalCCJob',
      start_date      => NULL,
      repeat_interval => 'FREQ=DAILY;byhour=23;byminute=0;bysecond=0',
      auto_drop       =>  FALSE,
      comments        => 'Eval replication movie',
      enabled         => FALSE);
  dbms_scheduler.enable('job_eval');
  commit;
end;
/

begin
  dbms_scheduler.run_job('job_eval');
end;
begin
  dbms_scheduler.drop_job(job_name => 'job_eval');
end;

  select * from user_scheduler_job_log order by log_date desc;
select * from (select * from user_scheduler_job_log order by log_date desc) where rownum < 3;
  select * from user_scheduler_job_run_details order by log_date desc;
select * from (select * from user_scheduler_job_run_details order by log_date desc) where JOB_NAME='job_eval';