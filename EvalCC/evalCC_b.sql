create or replace package body evalCC IS
    procedure eval(message varchar2) IS
        request_action VARCHAR2(10);
        request_movie VARCHAR2(10);
        request_username VARCHAR2(60);
        request_password VARCHAR2(60);
        request_vote NUMBER(2,0);
        request_avis VARCHAR(3000);
        user_Temp USER_ACCOUNT%ROWTYPE ; 
        avis_Temp USER_RATING%ROWTYPE ;
        nb_vote NUMBER(10,0);
        avg_vote NUMBER(3,1);
       
    BEGIN
        --PARSING JSON
        apex_json.parse(message);
        request_action := apex_json.get_varchar2('action');
        request_movie := apex_json.get_varchar2('movie');
        request_username := apex_json.get_varchar2('username');
        request_password := apex_json.get_varchar2('password');
        request_vote := apex_json.get_number('vote');
        request_avis := apex_json.get_varchar2('avis');
        --END PARSING JSON
    
        --TRAITEMENT DELETE / ADD            
        IF (request_action = 'delete')
        THEN
            BEGIN
                SELECT * INTO user_Temp FROM USER_ACCOUNT WHERE username = request_username AND password = request_password; 
                DELETE FROM USER_RATING WHERE userid = request_username AND movie = request_movie ;
                commit ;
            END ;
        ELSIF (request_action = 'add')
        THEN
            BEGIN
                SELECT * INTO user_Temp FROM USER_ACCOUNT WHERE username = request_username AND password = request_password;
                BEGIN
                    SELECT * INTO avis_Temp FROM USER_RATING WHERE userid = request_username AND movie = request_movie ;
                    UPDATE USER_RATING SET rate = request_vote, USER_RATING.description = request_avis, Date_modification = CURRENT_DATE WHERE userid = request_username AND movie = request_movie ;
                    COMMIT ;
                EXCEPTION
                    WHEN NO_DATA_FOUND
                    THEN
                        INSERT INTO USER_RATING VALUES(request_username, request_movie, request_vote, request_avis, CURRENT_DATE,0);
                        COMMIT ;
                END ;
            EXCEPTION
                WHEN NO_DATA_FOUND
                THEN
                    INSERT INTO USER_ACCOUNT VALUES(request_username, request_password);
                    INSERT INTO USER_RATING VALUES(request_username, request_movie, request_vote, request_avis, CURRENT_DATE,0);
                    COMMIT ; 
            END;
        END IF;
       --END TRAITEMENT DELETE / ADD 
    
        --CREATION REPONSE_JSON
        avg_vote := RECHCC_PKG.getAverageVotes(request_movie);
        nb_vote := RECHCC_PKG.getNumberVotes(request_movie);
        
        APEX_JSON.initialize_clob_output;
        APEX_JSON.open_object;
        
        APEX_JSON.write('rqs_votes_number', nb_vote);
        APEX_JSON.write('rqs_votes_average', avg_vote);
        
        APEX_JSON.close_object;
        --END CREATION REPONSE_JSON
            
        --CONSTRUCTION REPONSE_HTTP
        owa_util.status_line(nstatus => 200, bclose_header => false);
        owa_util.mime_header(
            'text/html',
            bclose_header => true,
            ccharset => 'UTF-8'
        );
        --END CONSTRUCTION REPONSE_HTTP
        htp.print(APEX_JSON.get_clob_output);

    EXCEPTION
        WHEN OTHERS
        THEN
          LOG_PKG.LOGERREUR('ERROR evalCC.eval', true);
          ROLLBACK ;
    END eval ;
    
    
    procedure avis(message varchar2) IS
        request_movie VARCHAR2(10);
        request_username VARCHAR2(60);
        user_vote NUMBER(3,0);
        user_avis VARCHAR(3000); 
        avis_Temp USER_RATING%ROWTYPE ;
    BEGIN
    --PARSING JSON
        apex_json.parse(message);
        request_movie := apex_json.get_varchar2('movie');
        request_username := apex_json.get_varchar2('username');
        --END PARSING JSON
    
        BEGIN
            SELECT * INTO avis_Temp FROM USER_RATING where userid = request_username AND movie = request_movie ;
            user_vote := avis_Temp.Rate;
            user_avis := avis_Temp.Description ;
            
        EXCEPTION
        WHEN NO_DATA_FOUND
        THEN 
            user_vote := -1 ;
            user_avis := ' ' ;
        END;
    
        --CREATION REPONSE_JSON
        if(user_avis is NULL) 
        then 
            user_avis := ' ' ;
        end if ;
        APEX_JSON.initialize_clob_output;
        APEX_JSON.open_object;
        
        APEX_JSON.write('vote', user_vote);
        APEX_JSON.write('avis', user_avis);
        
        APEX_JSON.close_object;
        --END CREATION REPONSE_JSON
            
        --CONSTRUCTION REPONSE_HTTP
        owa_util.status_line(nstatus => 200, bclose_header => false);
        owa_util.mime_header(
            'text/html',
            bclose_header => true,
            ccharset => 'UTF-8'
        );
        --END CONSTRUCTION REPONSE_HTTP
        htp.print(APEX_JSON.get_clob_output);

    EXCEPTION
        WHEN OTHERS
        THEN
            LOG_PKG.LOGERREUR('ERROR evalCC.avis', true);
    END avis;

    procedure evalCCJob is
  begin

    begin
    merge into USER_ACCOUNT@cblink uacb
      using(select * from USER_ACCOUNT) uacc
      on (uacc.USERNAME = uacb.USERNAME)
    when not matched then
      insert (uacb.USERNAME,uacb.PASSWORD)
        values (uacc.USERNAME,uacc.PASSWORD);
      exception
      when others
      then
      LOG_PKG.LOGERREUR('evalCCJob error on user_account merge',true);
      end;

    begin
      merge into USER_RATING@cblink urcb
        using(select * from USER_RATING) urcc
        on (urcc.USERID = urcb.USERID AND urcc.MOVIE = urcb.movie)
      when not matched then
        insert (urcb.USERID,urcb.MOVIE,urcb.RATE,urcb.DESCRIPTION,urcb.DATE_MODIFICATION,flag)
          values (urcc.USERID,urcc.MOVIE,urcc.RATE,urcc.DESCRIPTION,urcc.DATE_MODIFICATION,0);
      exception
      when others
      then
      LOG_PKG.LOGERREUR('evalCCJob error on user_rating merge',true);
    end;

    commit;

  end evalCCJob;

END evalCC;
/