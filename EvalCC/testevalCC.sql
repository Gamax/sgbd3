insert into USER_ACCOUNT values ('Thomas','pwd');
insert into USER_ACCOUNT values ('Steph','pwd');
insert into USER_ACCOUNT values ('Noe','pwd');
insert into USER_RATING values ('Thomas',13,6,'YOLO42',current_date);
insert into USER_RATING values ('Steph',13,7,'YOLO',current_date);
insert into USER_RATING values ('Noe',13,3,'YOLO',current_date);
commit;

delete USER_RATING where userid='Thomas';

select avg(RATE) from user_rating@cblink where movie = 12;
select count(RATE) from user_rating@cblink where movie = 13;

delete from USER_RATING@cblink where 'Thomas' = USERID
                                       and 13 = MOVIE;

select * from USER_RATING@cblink where 13 = movie;

create or replace procedure evalCCJob is
  begin

    begin
    merge into USER_ACCOUNT@cblink uacb
      using(select * from USER_ACCOUNT) uacc
      on (uacc.USERNAME = uacb.USERNAME)
    when not matched then
      insert (uacb.USERNAME,uacb.PASSWORD)
        values (uacc.USERNAME,uacc.PASSWORD);
      exception
      when others
      then
      LOG_PKG.LOGERREUR('evalCCJob error on user_account merge',true);
      end;

    begin
      merge into USER_RATING@cblink urcb
        using(select * from USER_RATING) urcc
        on (urcc.USERID = urcb.USERID AND urcc.MOVIE = urcb.movie)
      when not matched then
        insert (urcb.USERID,urcb.MOVIE,urcb.RATE,urcb.DESCRIPTION,urcb.DATE_MODIFICATION)
          values (urcc.USERID,urcc.MOVIE,urcc.RATE,urcc.DESCRIPTION,urcc.DATE_MODIFICATION);
      exception
      when others
      then
      LOG_PKG.LOGERREUR('evalCCJob error on user_rating merge',true);
    end;

    commit;

  end evalCCJob;